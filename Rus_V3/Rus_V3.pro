#-------------------------------------------------
#
# Project created by QtCreator 2019-02-21T16:34:02
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Rus_V3
TEMPLATE = app



INCLUDEPATH += Communication\
        Communication/Modbus \
        Communication/Modbus/libmodbus\
        Communication/Modbus/libmodbus/src \
        Communication/Postman \
        DialogWindows \
        MainControlWidget \
        MainControlWidget/CellWidget \
        MainControlWidget/centralWidgets \
        TimeSettings

FORMS += \
    DialogWindows/checkdeviceconnection.ui \
    DialogWindows/dialog.ui \
    DialogWindows/dialogonebtn.ui \
    MainControlWidget/CellWidget/battmodule.ui \
    MainControlWidget/CellWidget/celloab.ui \
    MainControlWidget/CellWidget/cellvab1.ui \
    MainControlWidget/CellWidget/cellvab2.ui \
    MainControlWidget/CellWidget/infocell.ui \
    MainControlWidget/centralWidgets/amperwidget.ui \
    MainControlWidget/centralWidgets/percentwidget.ui \
    MainControlWidget/centralWidgets/sourcemode.ui \
    MainControlWidget/controlmodulewidget.ui \
    MainControlWidget/controlwindow.ui \
    MainControlWidget/diagnosticwindow.ui \
    MainControlWidget/maincontrolwidget.ui \
    TimeSettings/spinwidget.ui \
    TimeSettings/timesettings.ui \
    mainwindow.ui

HEADERS += \
    Communication/Modbus/libmodbus/config.h \
    Communication/Modbus/modbussettings.h \
    Communication/Modbus/serialport.h \
    Communication/Postman/bms.h \
    Communication/Postman/chargemodule.h \
    Communication/Postman/mainpostman.h \
    Communication/Postman/modulecontrolevab2.h \
    Communication/Postman/modulecontroloabvab.h \
    DialogWindows/checkdeviceconnection.h \
    DialogWindows/dialog.h \
    DialogWindows/dialogonebtn.h \
    MainControlWidget/CellWidget/battmodule.h \
    MainControlWidget/CellWidget/celloab.h \
    MainControlWidget/CellWidget/cellvab1.h \
    MainControlWidget/CellWidget/cellvab2.h \
    MainControlWidget/CellWidget/infocell.h \
    MainControlWidget/centralWidgets/amperwidget.h \
    MainControlWidget/centralWidgets/capacitive_characteristic.h \
    MainControlWidget/centralWidgets/percentwidget.h \
    MainControlWidget/centralWidgets/sourcemode.h \
    MainControlWidget/controlmodulewidget.h \
    MainControlWidget/controlwindow.h \
    MainControlWidget/diagnosticbutton.h \
    MainControlWidget/diagnosticwindow.h \
    MainControlWidget/maincontrolwidget.h \
    TimeSettings/spinwidget.h \
    TimeSettings/timesettings.h \
    datastore.h \
    mainwindow.h \
    RusNamespace.h

SOURCES += \
    Communication/Modbus/modbussettings.cpp \
    Communication/Modbus/serialport.cpp \
    Communication/Postman/bms.cpp \
    Communication/Postman/chargemodule.cpp \
    Communication/Postman/mainpostman.cpp \
    Communication/Postman/modulecontrolevab2.cpp \
    Communication/Postman/modulecontroloabvab.cpp \
    DialogWindows/checkdeviceconnection.cpp \
    DialogWindows/dialog.cpp \
    DialogWindows/dialogonebtn.cpp \
    MainControlWidget/CellWidget/battmodule.cpp \
    MainControlWidget/CellWidget/celloab.cpp \
    MainControlWidget/CellWidget/cellvab1.cpp \
    MainControlWidget/CellWidget/cellvab2.cpp \
    MainControlWidget/CellWidget/infocell.cpp \
    MainControlWidget/centralWidgets/amperwidget.cpp \
    MainControlWidget/centralWidgets/capacitive_characteristic.cpp \
    MainControlWidget/centralWidgets/percentwidget.cpp \
    MainControlWidget/centralWidgets/sourcemode.cpp \
    MainControlWidget/controlmodulewidget.cpp \
    MainControlWidget/controlwindow.cpp \
    MainControlWidget/diagnosticbutton.cpp \
    MainControlWidget/diagnosticwindow.cpp \
    MainControlWidget/maincontrolwidget.cpp \
    TimeSettings/spinwidget.cpp \
    TimeSettings/timesettings.cpp \
    datastore.cpp \
    main.cpp \
    mainwindow.cpp \
    Communication/Modbus/libmodbus/src/modbus-ascii.c \
    Communication/Modbus/libmodbus/src/modbus-data.c \
    Communication/Modbus/libmodbus/src/modbus-rtu.c \
    Communication/Modbus/libmodbus/src/modbus-tcp.c \
    Communication/Modbus/libmodbus/src/modbus.c




