#ifndef DATASTORE_H
#define DATASTORE_H

#include <QMutex>

struct dsChargeModule
{
    QMutex mutex;

    bool connectionStatus = false;
    bool connectionStatusTwo = false;

    int infoRegOne = 0;
    int infoRegTwo = 0;
    int voltageOne = 0;
    int voltageTwo = 0;
    int currentOne = 0;
    int currentTwo = 0;
    int workeModeOne = 0;
    int workeModeTwo = 0;
    int u_voltage = 0;
};

struct dsModuleControle
{
    QMutex mutex;

    int voltageOne = 0;
    int currentOne = 0;
    int dbus = 0;
    int voltageTwo = 0;
    int currentTwo = 0;
    int bmsFlags= 0 ;
    int chargeMode= 0;

    bool connectionStatus = false;

};

struct dsBattManagSystem
{
    QMutex mutex;

    int voltageOne = 0;
    int currentOne = 0;
    int dbus = 0;
    int voltageTwo = 0;
    int currentTwo = 0;
    int bmsFlags = 0;
    int chargeMode = 0;

    bool connectionStatus = false;

};


const int maxCell =  48;

struct dsBatteryCell
{
    int voltage = 0;
    int temp_0 = 0;
    int temp_1 = 0;
    int infoReg = 0;
    int section = 0;

    int flagErrors;
};

struct dsBattery
{
    QMutex mutex;
    dsBatteryCell cell[maxCell];
};

struct dsFlagsDiagnostic
{
    QMutex mutex;

    bool connectionStatus;
    bool connectionStatusCharger;
    bool validCells;
    bool highVoltage;
    bool lowVoltage;
    bool highTemp;
    bool lowTemp;
    bool dumpSensor;

    int flagErrorsBatt;
};

class DataStore
{
public:
    DataStore();
};

#endif // DATASTORE_H
