#include "modbussettings.h"
#include "mainwindow.h"
#include <errno.h>
#include <QDebug>
#include <QThread>


ModbusSettings::ModbusSettings(QObject *parent) :
    QObject(parent)
{
    SerialPort = new serialport(this);
    m_modbus=nullptr;

    registers.fill(0,10);
}

ModbusSettings::~ModbusSettings()
{
}

void ModbusSettings::createRtuModbusConnection(const QString jsonPortName){
    portName=jsonPortName;
    if(SerialPort->connectRtuModbusDevice(jsonPortName)){
        m_modbus = SerialPort->modbus();
    } else {
        m_modbus = nullptr;
    }
}

void ModbusSettings::getRegisters(Message settingParamentrs){

    if(!m_modbus) {
        createRtuModbusConnection(portName);
        return;
    }

    mutex.lock();

    u_int16_t dest[settingParamentrs.NumbersRegisters*2];
    memset( dest, 0, settingParamentrs.NumbersRegisters*2 );

    try
    {
        modbus_set_slave(m_modbus,settingParamentrs.SlaveID);

        int ret = -1;
        ret = modbus_read_registers( m_modbus,
                                     settingParamentrs.StartRegister,
                                     settingParamentrs.NumbersRegisters,
                                     dest);

        if(ret != settingParamentrs.NumbersRegisters) throw ret;

        for (int var = 0; var < registers.count(); ++var) {
           registers[var] = dest[var];
        }

        retriers = 0;
        for (int var = 0; var < ret; ++var)
        {
            registers[var] = dest[var];
        }
        checkConnection = true;
        emit registersRead(registers);
        //qDebug() << settingParamentrs.SlaveID << " - " <<registers;
    }
    catch (int ret)
    {
        checkConnection = false;
        if (ret < 0) destroyModbusDevice();
    }
    mutex.unlock();
}

void ModbusSettings::setRegisters(Message settingParamentrs){
    if(!m_modbus) {
        createRtuModbusConnection(portName);
        return;
    }
    mutex.lock();

    try{
        modbus_set_slave(m_modbus,settingParamentrs.SlaveID);

        int ret = -1;
        ret = modbus_write_register(m_modbus,
                                    settingParamentrs.StartRegister,
                                    settingParamentrs.ValueInRegister);
        if(ret != settingParamentrs.NumbersRegisters) throw ret;
        retriers = 0;
        checkConnection = true;
    }
    catch (int ret){
        checkConnection = false;
        if (ret < 0) destroyModbusDevice();
    }
    mutex.unlock();
}

void ModbusSettings::destroyModbusDevice(){  
    retriers++;
    if(retriers == 5){
        m_modbus = nullptr;
        retriers = 0;
    }
}
