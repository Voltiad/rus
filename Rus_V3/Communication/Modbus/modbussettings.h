 #ifndef MODBUSSETTINGS_H
#define MODBUSSETTINGS_H

#include <QWidget>
#include <QMutex>
#include <QTime>

#include "serialport.h"
#include "modbus.h"


class Message
{
public:
    u_int16_t SlaveID;           // Стартовый регистр
    u_int16_t StartRegister;     // Стартовый регистр
    u_int16_t NumbersRegisters;  // Количество регистров
    u_int16_t ValueInRegister;   // Число, которое нужно записать в регистр

    explicit Message(){}

    void setParametrs(u_int16_t writeSlaveID,
                      u_int16_t writeStartRegister,
                      u_int16_t writeNumbersRegisters,
                      u_int16_t writeValueInRegister)
    {
        SlaveID = writeSlaveID;
        StartRegister = writeStartRegister;
        NumbersRegisters = writeNumbersRegisters;
        ValueInRegister = writeValueInRegister;
    }

};

class ModbusSettings : public QObject
{
   Q_OBJECT
public:
    explicit ModbusSettings(QObject *parent = 0);
        ~ModbusSettings();
public slots:
    void createRtuModbusConnection(const QString jsonPortName);
    void getRegisters(Message);
    void setRegisters(Message);

private:
    void destroyModbusDevice();
    QTime *time;

public:
   QVector <short> registers;

private:
    serialport *SerialPort;
    modbus_t * m_modbus;
    QString portName;
    QMutex mutex;

    int retriers = 0;

signals:
    void registersRead(QVector <short>);

public:
    bool checkConnection;
};

#endif // MODBUSSETTINGS_H
