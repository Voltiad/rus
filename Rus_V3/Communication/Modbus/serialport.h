#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QWidget>
#include <QObject>
#include <QSettings>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QDebug>

#include "modbus.h"

class serialport : public QObject
{
    Q_OBJECT

public:
    explicit serialport(QObject *parent = 0);
    ~serialport();
    virtual modbus_t*  modbus(){ return m_serialModbus; }

    bool connectRtuModbusDevice(const QString jsonPortName);
    void setEchoMode(int mode);
private:
    bool readJsonComParameters();
    bool connectToSerialPort();
    void resetSerialModbus();

protected:
    modbus_t *     m_serialModbus;

private:
    QString comNameOnBody;
    QString comName;
    int baudRateJson;
    int stopBitsJson;
    int dataBitsJson;
    QString parityJson;
    int echoMode;

};

#endif // SERIALPORT_H
