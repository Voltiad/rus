#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QCoreApplication>
#include <QDebug>
#include <QJsonArray>

#include "serialport.h"
#include "mainwindow.h"
#include "RusNamespace.h"


serialport::serialport(QObject *parent) :
    QObject(parent)
{
    m_serialModbus = nullptr;
}

serialport::~serialport()
{

}

bool serialport::connectRtuModbusDevice(const QString jsonPortName)
{
    comNameOnBody = jsonPortName;

    if(readJsonComParameters())
    {
        if(connectToSerialPort())
            return true;
        else
            return false;
    }
    else
        return false;
}

void serialport::setEchoMode(int mode)
{
    echoMode = mode;
}

bool serialport::readJsonComParameters()
{
    QFile configFile (QCoreApplication::applicationDirPath() +
                      PATH_JSON +
                      CONFIG_FILE_NAME);
    QJsonDocument jsonDoc;

    if(configFile.exists())
    {
        configFile.open(QFile::ReadOnly);

        jsonDoc = QJsonDocument().fromJson(configFile.readAll());
        QJsonObject obj =jsonDoc.object();

        QJsonValue jsonValue = obj.value(comNameOnBody);

        if(jsonValue.isArray()){
            QJsonArray jsonArray = jsonValue.toArray();
            for (int var = 0; var < jsonArray.count(); ++var) {
                QJsonObject subtree = jsonArray.at(var).toObject();

                comName         = subtree.value(comNameOnBody).toString();
                baudRateJson    = subtree.value("Baud").toInt();
                stopBitsJson    = subtree.value("StopB").toInt();
                dataBitsJson    = subtree.value("DataB").toInt();
                parityJson      = subtree.value("Parity").toString();
                echoMode        = subtree.value("echoMode").toInt();

            }
        }
        configFile.close();
        return true;
    }
    else
       return false;
}

bool serialport::connectToSerialPort()
{
   resetSerialModbus();


   m_serialModbus = modbus_new_rtu(
            comName.toLatin1().constData(),
            baudRateJson,
            parityJson[0].toLatin1(),
            dataBitsJson,
            stopBitsJson,
            echoMode);

    if( modbus_connect( m_serialModbus ) == -1 )
    {
        //qDebug() << "Could not connect serial port " << comNameOnBody;
        resetSerialModbus();
    }

    if(m_serialModbus)
    {
        qDebug()<< "Port " << comNameOnBody << " open to r/w";
        return true;
    }
    else
        return false;

}

void serialport::resetSerialModbus()
{
    if( m_serialModbus )
    {
        modbus_close( m_serialModbus );
        modbus_free( m_serialModbus );
        m_serialModbus = nullptr;
    }

}
