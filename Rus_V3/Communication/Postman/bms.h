#ifndef BMS_H
#define BMS_H

#include "mainpostman.h"

class BMS : public MainPostMan
{
public:
    BMS(QString = "");

    QTimer *timer;

public slots:
    void communicationWithDevices();

    void start();
    void stop();

    void exchangeCycle();

    void getDefBMS();

    void getDefBatt();

    void startBalanceBMS();
    void stopBalanceBMS();

    void updatePresets(int value);


private:
    QString jsonPortName;

    bool flagGetDefBMS;
    bool flagGetDefBatt;

    bool flagStartBalanceBMS;
    bool flagStopBalanceBMS;

    int delay;
};

#endif // BMS_H
