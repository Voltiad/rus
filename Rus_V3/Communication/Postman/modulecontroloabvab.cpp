#include "modulecontroloabvab.h"
#include "RusNamespace.h"
#include <QThread>
#include <QDebug>

moduleControlOABVAB::moduleControlOABVAB(QString jsonName)
{
    jsonPortName = jsonName;

    MainPostMan::setPortName(jsonName);

    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),
            this,SLOT(exchangeCycle()));

    delay = 10;

    cntMcOAB = 0;
    tryMcOAB = 0;

    cntMcVAB1 = 0;
    tryMcVAB1 = 0;

}

void moduleControlOABVAB::communicationWithDevices()
{
    if(flagGetMcInfoOAB)
    {  flagGetMcInfoOAB = false;  }
    if(flagGetMcInfoVAB)
    {  flagGetMcInfoVAB = false;  }
    if(flagGetBattMc)
    {   flagGetBattMc = false;}

    if(flagStartDischargeOAB)
    {  flagStartDischargeOAB = false;  }
    if(flagStartDischargeVAB1)
    {  flagStartDischargeVAB1 = false;  }
    if(flagStopDischargeOAB)
    {   flagStopDischargeOAB = false;}
    if(flagStopDischargeVAB1)
    {  flagStopDischargeVAB1 = false;  }

    if(flagStartBalanceOAB)
    {  flagStartBalanceOAB = false;  }
    if(flagStartBalanceVAB1)
    {   flagStartBalanceVAB1 = false;}
    if(flagStopBalanceOAB)
    {  flagStopBalanceOAB = false;  }
    if(flagStopBalanceVAB1)
    {   flagStopBalanceVAB1 = false;}


    MainPostMan::communicationWithDevices();
}

void moduleControlOABVAB::start()
{
    timer->start(msectmr);
}

void moduleControlOABVAB::stop()
{
    timer->stop();
}

void moduleControlOABVAB::exchangeCycle()
{
    getMcInfoOAB();
    QThread::msleep(20);
    getMcInfoVAB1();
    QThread::msleep(20);

    getBattMC();
    QThread::msleep(20);
}

void moduleControlOABVAB::getMcInfoOAB()
{
    flagGetMcInfoVAB = true;


    message.setParametrs(adrMcOAB,startAdrBMS,defInoRegBMS,0);
    ModbusDevice->getRegisters(message);

    moduleCOAB->mutex.lock();

    if(ModbusDevice->checkConnection == false)
    {
         moduleCOAB->connectionStatus = false;
    }
    else
    {
        moduleCOAB->connectionStatus = true;
        moduleCOAB->voltageOne = receiveMessage[0];
        moduleCOAB->currentOne = receiveMessage[1];
        moduleCOAB->dbus = receiveMessage[2];
        moduleCOAB->bmsFlags = receiveMessage[6];
    }

   moduleCOAB->mutex.unlock();

    MainPostMan::getMcInfoOAB();
}

void moduleControlOABVAB::getMcInfoVAB1()
{
    flagGetMcInfoVAB = true;

    message.setParametrs(adrMcOVAB1,startAdrBMS,defInoRegBMS,0);
    ModbusDevice->getRegisters(message);

    moduleCVAB1->mutex.lock();

    if(ModbusDevice->checkConnection == false)
    {
        moduleCVAB1->connectionStatus = false;
    }
    else
    {
        moduleCVAB1->connectionStatus = true;
        moduleCVAB1->voltageOne = receiveMessage[0];
        moduleCVAB1->currentOne = receiveMessage[1];
        moduleCVAB1->dbus = receiveMessage[2];
        moduleCVAB1->bmsFlags = receiveMessage[6];
    }
    moduleCVAB1->mutex.unlock();


    MainPostMan::getMcInfoVAB1();
}

void moduleControlOABVAB::getBattMC()
{
   flagGetBattMc = true;

   if(moduleCOAB->connectionStatus == true)
   {
       getBattOAB();
   }

   if(moduleCVAB1->connectionStatus == true)
   {
       getBattVAB1();
   }

   MainPostMan::getBattMC();
}

void moduleControlOABVAB::getBattOAB()
{
    if(getNumMode() == RS::LiOnDischarge)
    {
           for (int var = 0; var < 32; ++var)
           {
               int startAdr = startAdrBMSBat + defInoRegBMSBat*var;
                message.setParametrs(adrMcOAB,startAdr,defInoRegBMSBat,0);
                ModbusDevice->getRegisters(message);

                moduleControleBattery->mutex.lock();

                moduleControleBattery->cell[var].voltage = receiveMessage[0];
                moduleControleBattery->cell[var].temp_0 = receiveMessage[1];
                moduleControleBattery->cell[var].temp_1 = receiveMessage[2];
                moduleControleBattery->cell[var].infoReg = receiveMessage[3];
                moduleControleBattery->cell[var].section = receiveMessage[4];

                moduleControleBattery->mutex.unlock();

               QThread::msleep(10);
           }
    }

}

void moduleControlOABVAB::getBattVAB1()
{

    int startVar;
    int stopVar;

    if(getNumMode() == RS::LiOnChargeBalanceModule ||
            getNumMode() == RS::LiOnChargeNoBalanceModule ||
            getNumMode() == RS::LiOnDischarge)
    {

        startVar = 0;
        stopVar = 8;
    }
    else
    {
        startVar = 40;
        stopVar = 48;
    }

    if(startVar == 0)
    {
        for (int var = startVar; var < stopVar; ++var)
        {
            int startAdr = startAdrBMSBat + defInoRegBMSBat*var;
            message.setParametrs(adrMcOVAB1,startAdr,defInoRegBMSBat,0);

            ModbusDevice->getRegisters(message);

            moduleControleBattery->mutex.lock();

            moduleControleBattery->cell[var+40].voltage = receiveMessage[0];
            moduleControleBattery->cell[var+40].temp_0 = receiveMessage[1];
            moduleControleBattery->cell[var+40].temp_1 = receiveMessage[2];
            moduleControleBattery->cell[var+40].infoReg = receiveMessage[3];
            moduleControleBattery->cell[var+40].section = receiveMessage[4];

            moduleControleBattery->mutex.unlock();
            QThread::msleep(10);

        }
    }
    else
    {
        for (int var = startVar; var < stopVar; ++var)
        {
            int startAdr = startAdrBMSBat + defInoRegBMSBat*var;
             message.setParametrs(adrMcOVAB1,startAdr,defInoRegBMSBat,0);

             ModbusDevice->getRegisters(message);

             moduleControleBattery->mutex.lock();

             moduleControleBattery->cell[var].voltage = receiveMessage[0];
             moduleControleBattery->cell[var].temp_0 = receiveMessage[1];
             moduleControleBattery->cell[var].temp_1 = receiveMessage[2];
             moduleControleBattery->cell[var].infoReg = receiveMessage[3];
             moduleControleBattery->cell[var].section = receiveMessage[4];

             moduleControleBattery->mutex.unlock();
             QThread::msleep(10);
        }
    }
}


void moduleControlOABVAB::startDischargeOAB()
{
    flagStartDischargeOAB = false;

    message.setParametrs(adrMcOAB,1322,0,2);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startDischargeOAB();
}

void moduleControlOABVAB::startDischargeVAB1()
{
    flagStartDischargeVAB1 = false;

    message.setParametrs(adrMcOVAB1,1322,0,2);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startDischargeVAB1();
}

void moduleControlOABVAB::stopDischargeOAB()
{
    flagStopDischargeOAB = false;


    message.setParametrs(adrMcOAB,1322,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::stopDischargeOAB();
}

void moduleControlOABVAB::stopDischargeVAB1()
{
    flagStopDischargeVAB1 = false;

    message.setParametrs(adrMcOVAB1,1322,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::stopDischargeVAB1();
}

void moduleControlOABVAB::startBalanceOAB()
{
    flagStartBalanceOAB = false;

    message.setParametrs(adrMcOAB,1327,0,1);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startBalanceOAB();
}

void moduleControlOABVAB::startBalanceVAB1()
{
    flagStartBalanceVAB1 = false;


    message.setParametrs(adrMcOVAB1,1327,0,1);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startBalanceVAB1();
}

void moduleControlOABVAB::stopBalanceOAB()
{
    flagStopBalanceOAB = false;

    message.setParametrs(adrMcOAB,1327,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::stopBalanceOAB();
}

void moduleControlOABVAB::stopBalanceVAB1()
{
    flagStopBalanceVAB1 = false;

    message.setParametrs(adrMcOVAB1,1327,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::stopBalanceVAB1();
}

void moduleControlOABVAB::updatePresets(int index)
{
    stopBalanceOAB();
    stopBalanceVAB1();
    stopDischargeOAB();
    stopBalanceVAB1();
    MainPostMan::updatePresets(0);
}

