#include "bms.h"
#include <RusNamespace.h>
#include <QThread>
#include <QDebug>

BMS::BMS(QString jsonName)
{
    jsonPortName = jsonName;

    MainPostMan::setPortName(jsonName);

    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(exchangeCycle()));

    delay = 10;
    cntBms = 0;
    tryBms = 0;

}

void BMS::communicationWithDevices()
{
    if(flagGetDefBMS)
    {  flagGetDefBMS = false;  }
    if(flagGetDefBatt)
    {  flagGetDefBatt = false;  }

    if(flagStartBalanceBMS){
        flagStartBalanceBMS = false;
    }
    if(flagStopBalanceBMS){
        flagStopBalanceBMS = false;
    }

    MainPostMan::communicationWithDevices();
}

void BMS::start()
{
    timer->start(msectmr);
}

void BMS::stop()
{
    timer->stop();
}

void BMS::exchangeCycle()
{
    getDefBMS();
    QThread::msleep(20);

    if(battManagSystem->connectionStatus == true)
    {
        getDefBatt();
        QThread::msleep(20);
    }


    MainPostMan::exchangeCycle();
}

void BMS::getDefBMS()
{
    flagGetDefBMS = true;

    message.setParametrs(adrBMS,startAdrBMS,defInoRegBMS,0);
    ModbusDevice->getRegisters(message);

    battManagSystem->mutex.lock();

    if(ModbusDevice->checkConnection == false)
    {
        battManagSystem->connectionStatus = false;
    }
    else
    {
        battManagSystem->connectionStatus = true;

        battManagSystem->voltageOne = receiveMessage[0];
        battManagSystem->currentOne = receiveMessage[1];
        battManagSystem->dbus = receiveMessage[2];
        battManagSystem->voltageTwo = receiveMessage[3];
        battManagSystem->currentTwo = receiveMessage[4];
        battManagSystem->bmsFlags = receiveMessage[6];
        battManagSystem->chargeMode = receiveMessage[7];

    }
    battManagSystem->mutex.unlock();


    MainPostMan::getDefBMS();
}

void BMS::getDefBatt()
{
    flagGetDefBatt = true;

    for (int var = 0; var < maxCell; ++var)
    {
        int startAdr = startAdrBMSBat + defInoRegBMSBat*var;

        message.setParametrs(adrBMS,startAdr,defInoRegBMSBat,0);
        ModbusDevice->getRegisters(message);

        bmsBattery->mutex.lock();
            bmsBattery->cell[var].voltage = receiveMessage[0];
            bmsBattery->cell[var].temp_0 = receiveMessage[1];
            bmsBattery->cell[var].temp_1 = receiveMessage[2];
            bmsBattery->cell[var].infoReg = receiveMessage[3];
            bmsBattery->cell[var].section = receiveMessage[4];
        bmsBattery->mutex.unlock();
        QThread::msleep(10);

    }

    MainPostMan::getDefBatt();
}

void BMS::startBalanceBMS()
{
    flagStartBalanceBMS = true;

    message.setParametrs(adrBMS,1327,0,1);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);


    MainPostMan::startBalanceBMS();
}

void BMS::stopBalanceBMS()
{
    flagStopBalanceBMS = true;

    message.setParametrs(adrBMS,1327,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);


    MainPostMan::stopBalanceBMS();
}

void BMS::updatePresets(int value)
{
    stopBalanceBMS();
    MainPostMan::updatePresets(0);
}
