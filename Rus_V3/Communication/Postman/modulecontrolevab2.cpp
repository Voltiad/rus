#include "modulecontrolevab2.h"
#include "RusNamespace.h"
#include <QThread>
#include <QDebug>

moduleControleVAB2::moduleControleVAB2(QString jsonName)
{
    jsonPortName = jsonName;

    MainPostMan::setPortName(jsonName);

    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),
            this,SLOT(exchangeCycle()));

    delay = 10;

    cntMcVAB2 = 0;
    tryMcVAB2 = 0;
}

void moduleControleVAB2::communicationWithDevices()
{
    if(flagGetMcInfoVAB2)
    {  flagGetMcInfoVAB2 = false;  }
    if(flagGetBattMc)
    {   flagGetBattMc = false;}

    if(flagStartDischargeVAB2)
    {  flagStartDischargeVAB2 = false;  }
    if(flagStopDischargeVAB2)
    {   flagStopDischargeVAB2 = false;}
    if(flagStartBalanceVAB2)
    {  flagStartBalanceVAB2 = false;  }
    if(flagStopBalanceVAB2)
    {   flagStopBalanceVAB2 = false;}

    MainPostMan::communicationWithDevices();
}

void moduleControleVAB2::start()
{
    timer->start(msectmr);
}

void moduleControleVAB2::stop()
{
    timer->stop();
}

void moduleControleVAB2::exchangeCycle()
{

    getMcInfoVAB2();
    QThread::msleep(20);

    if(moduleCVAB2->connectionStatus == true)
    {
        getBattMC();
        QThread::msleep(20);
    }
}

void moduleControleVAB2::getMcInfoVAB2()
{

    flagGetMcInfoVAB2 = true;

    message.setParametrs(adrMcVAB2,startAdrBMS,defInoRegBMS,0);
    ModbusDevice->getRegisters(message);

    moduleCVAB2->mutex.lock();

    if(ModbusDevice->checkConnection == false)
    {
        moduleCVAB2->connectionStatus = false;
    }
    else
    {
        moduleCVAB2->connectionStatus = true;
        moduleCVAB2->voltageOne = receiveMessage[0];
        moduleCVAB2->currentOne = receiveMessage[1];
        moduleCVAB2->dbus = receiveMessage[2];
        moduleCVAB2->bmsFlags = receiveMessage[6];
    }


    moduleCVAB2->mutex.unlock();


    MainPostMan::getMcInfoVAB2();

}

void moduleControleVAB2::getBattMC()
{
    flagGetBattMc = true;

    int startVar;
    int stopVar;

    if(getNumMode() == RS::LiOnChargeBalanceModule ||
            getNumMode() == RS::LiOnChargeNoBalanceModule ||
            getNumMode() == RS::LiOnDischarge)
    {

        startVar = 0;
        stopVar = 8;
    }
    else
    {
        startVar = 32;
        stopVar = 40;
    }

    for (int var = startVar; var < stopVar; ++var)
    {
        int startAdr = startAdrBMSBat + defInoRegBMSBat*var;
        message.setParametrs(adrMcVAB2,startAdr,defInoRegBMSBat,0);
        ModbusDevice->getRegisters(message);

            if(startVar == 0)
            {
                moduleControleBattery->mutex.lock();
                moduleControleBattery->cell[var+32].voltage = receiveMessage[0];
                moduleControleBattery->cell[var+32].temp_0 = receiveMessage[1];
                moduleControleBattery->cell[var+32].temp_1 = receiveMessage[2];
                moduleControleBattery->cell[var+32].infoReg = receiveMessage[3];
                moduleControleBattery->cell[var+32].section = receiveMessage[4];
                moduleControleBattery->mutex.unlock();
                QThread::msleep(10);
            }
            else
            {
                moduleControleBattery->mutex.lock();
                moduleControleBattery->cell[var].voltage = receiveMessage[0];
                moduleControleBattery->cell[var].temp_0 = receiveMessage[1];
                moduleControleBattery->cell[var].temp_1 = receiveMessage[2];
                moduleControleBattery->cell[var].infoReg = receiveMessage[3];
                moduleControleBattery->cell[var].section = receiveMessage[4];
                moduleControleBattery->mutex.unlock();
                QThread::msleep(10);
            }
    }

    MainPostMan::getBattMC();
}

void moduleControleVAB2::startDischargeVAB2()
{
    flagStartDischargeVAB2 = true;

    message.setParametrs(adrMcVAB2,1322,0,2);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startDischargeVAB2()   ;
}

void moduleControleVAB2::stopDischargeVAB2()
{
    flagStopDischargeVAB2 = true;

    message.setParametrs(adrMcVAB2,1322,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::stopDischargeVAB2();
}

void moduleControleVAB2::startBalanceVAB2()
{
    flagStartBalanceVAB2 = true;

    message.setParametrs(adrMcVAB2,1327,0,1);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startBalanceVAB2();
}

void moduleControleVAB2::stopBalanceVAB2()
{
    flagStopBalanceVAB2 = true;

    message.setParametrs(adrMcVAB2,1327,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::stopBalanceVAB2();
}

void moduleControleVAB2::updatePresets(int index)
{
    stopBalanceVAB2();
    stopDischargeVAB2();
    MainPostMan::updatePresets(0);
}
