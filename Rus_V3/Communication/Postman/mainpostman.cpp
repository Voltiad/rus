#include "mainpostman.h"

#include <QTime>
#include <QTimer>
#include <QDebug>

MainPostMan::MainPostMan(QObject *parent) :
    QObject(parent)
{
    ModbusDevice = new ModbusSettings();

    connect(ModbusDevice,&ModbusSettings::registersRead,
            [this](QVector<short> recReg)
    {receiveMessage = recReg;});

    timeElapsed = new QTime();  
}

void MainPostMan::setInterval(int msec)
{
    msectmr = msec;
}

void MainPostMan::loop(){
    QEventLoop EventLoop;
    while(true){

        communicationWithDevices();
        EventLoop.processEvents();
    }
}
void MainPostMan::sleep(qint64 msec){

}
