#ifndef MODULECONTROLOABVAB_H
#define MODULECONTROLOABVAB_H

#include "mainpostman.h"

class moduleControlOABVAB : public MainPostMan
{
public:
    moduleControlOABVAB(QString = "");

    QTimer *timer;

public slots:
    void communicationWithDevices();
    void start();
    void stop();

    void exchangeCycle();

    void getMcInfoOAB();
    void getMcInfoVAB1();

    void getBattMC();


    void startDischargeOAB();
    void startDischargeVAB1();

    void stopDischargeOAB();
    void stopDischargeVAB1();

    void startBalanceOAB();
    void startBalanceVAB1();

    void stopBalanceOAB();
    void stopBalanceVAB1();


    void updatePresets(int index);

private:
    QString jsonPortName;
    bool flagExchangeCycle;

    bool flagGetMcInfoOAB;
    bool flagGetMcInfoVAB;

    bool flagGetBattMc;

    bool flagStartDischargeOAB;
    bool flagStartDischargeVAB1;

    bool flagStopDischargeOAB;
    bool flagStopDischargeVAB1;

    bool flagStartBalanceOAB;
    bool flagStartBalanceVAB1;

    bool flagStopBalanceOAB;
    bool flagStopBalanceVAB1;

    int delay;

    void getBattOAB();
    void getBattVAB1();
};

#endif // MODULECONTROLOABVAB_H
