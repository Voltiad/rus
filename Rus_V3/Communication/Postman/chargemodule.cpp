#include "chargemodule.h"
#include <QJsonArray>
#include <QThread>

ChargeModule::ChargeModule(QString jsonName)
{
    jsonPortName = jsonName;

    MainPostMan::setPortName(jsonName);

    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(exchangeCycle()));

    delay = 30;

    tryChargeOAB = 0;
    tryChargeOAB1 = 0;
    tryChargeVAB1 = 0;
    tryChargeVAB2 = 0;

    cntChargeOAB = 0;
    cntChargeOAB1 = 0;
    cntChargeVAB1 = 0;
    cntChargeVAB2 = 0;
}

void ChargeModule::communicationWithDevices()
{

    if(flagSetVltOAB)
    {   flagSetVltOAB = false;  }
    if(flagSetVltVAB1)
    {   flagSetVltVAB1 = false;  }
    if(flagSetVltVAB2)
    {   flagSetVltVAB2 = false;  }

    if(flagGetDefaultInfoOAB)
    {  flagGetDefaultInfoOAB = false;  }
    if(flagGetDefaultInfoVAB1)
    {  flagGetDefaultInfoVAB1 = false;  }
    if(flagGetDefaultInfoVAB2)
    {  flagGetDefaultInfoVAB2 = false;  }

    if(flagSetDefaultInfoOAB)
    {  flagSetDefaultInfoOAB = false;  }
    if(flagSetDefaultInfoVAB1)
    {  flagSetDefaultInfoVAB1 = false;  }
    if(flagSetDefaultInfoVAB2)
    {  flagSetDefaultInfoVAB2 = false;  }

    if(flagStartOAB)
    {  flagStartOAB = false;  }
    if(flagStartVAB1)
    {  flagStartVAB1 = false;  }
    if(flagStartVAB2)
    {  flagStartVAB2 = false;  }

    if(flagStopOAB)
    {  flagStopOAB = false;  }
    if(flagStopVAB1)
    {  flagStopVAB1 = false;  }
    if(flagStopVAB2)
    {  flagStopVAB2 = false;  }


    MainPostMan::communicationWithDevices();
}

void ChargeModule::start()
{
    timer->start(msectmr);
}

void ChargeModule::stop()
{
    timer->stop();
}

void ChargeModule::exchangeCycle(){

    getDefaultInfoOAB();
    QThread::msleep(delay);
    getDefaultInfoVAB1();
    QThread::msleep(delay);
    getDefaultInfoVAB2();
    QThread::msleep(delay);

    MainPostMan::exchangeCycle();
}

void ChargeModule::getDefaultInfoOAB()
{
    flagGetDefaultInfoOAB = true;

    infoOAB();
    QThread::msleep(20);
    infoOAB1();
    QThread::msleep(20);

    MainPostMan::getDefaultInfoOAB();
}

void ChargeModule::infoOAB()
{
    message.setParametrs(adrCharOAB,startAdrCharge,defInoRegCharge,0);
    ModbusDevice->getRegisters(message);

    chargerOAB->mutex.lock();
    if(ModbusDevice->checkConnection == false)
    {
        chargerOAB->connectionStatus = false;
    }
    else
    {
        chargerOAB->connectionStatus = true;
        chargerOAB->infoRegOne = receiveMessage[0];
        chargerOAB->voltageOne = receiveMessage[1];
        chargerOAB->currentOne = receiveMessage[2];
        chargerOAB->u_voltage = receiveMessage[7];
        chargerOAB->workeModeOne = receiveMessage[9];
    }
    chargerOAB->mutex.unlock();


}

void ChargeModule::infoOAB1()
{
    message.setParametrs(adrCharOAB1,startAdrCharge,defInoRegCharge,0);
    ModbusDevice->getRegisters(message);

    chargerOAB->mutex.lock();
    if(ModbusDevice->checkConnection == false)
    {
        chargerOAB->connectionStatusTwo = false;
    }
    else
    {
        chargerOAB->connectionStatusTwo = true;
        chargerOAB->infoRegTwo = receiveMessage[0];
        chargerOAB->voltageTwo = receiveMessage[1];
        chargerOAB->currentTwo = receiveMessage[2];
        chargerOAB->u_voltage = receiveMessage[7];
        chargerOAB->workeModeTwo = receiveMessage[9];      
    }
    chargerOAB->mutex.unlock();


}

void ChargeModule::getDefaultInfoVAB1()
{
    flagGetDefaultInfoVAB1 = true;

    message.setParametrs(adrCharVAB1,startAdrCharge,defInoRegCharge,0);
    ModbusDevice->getRegisters(message);

    chargerVAB1->mutex.lock();
    if(ModbusDevice->checkConnection == false)
    {
        chargerVAB1->connectionStatus = false;
    }
    else
    {
        chargerVAB1->connectionStatus = true;
        chargerVAB1->infoRegOne = receiveMessage[0];
        chargerVAB1->voltageOne = receiveMessage[1];
        chargerVAB1->currentOne = receiveMessage[2];
        chargerVAB1->u_voltage = receiveMessage[7];
        chargerVAB1->workeModeOne = receiveMessage[9];
    }

    chargerVAB1->mutex.unlock();

    MainPostMan::getDefaultInfoVAB1();
}

void ChargeModule::getDefaultInfoVAB2()
{
    flagGetDefaultInfoVAB2 = true;

    message.setParametrs(adrCharVAB2,startAdrCharge,defInoRegCharge,0);
    ModbusDevice->getRegisters(message);

    chargerVAB2->mutex.lock();
    if(ModbusDevice->checkConnection == false)
    {
        chargerVAB2->connectionStatus = false;
    }
    else
    {
        chargerVAB2->connectionStatus = true;
        chargerVAB2->infoRegOne = receiveMessage[0];
        chargerVAB2->voltageOne = receiveMessage[1];
        chargerVAB2->currentOne = receiveMessage[2];
        chargerVAB2->u_voltage = receiveMessage[7];
        chargerVAB2->workeModeOne = receiveMessage[9];
    }

    chargerVAB2->mutex.unlock();

    MainPostMan::getDefaultInfoVAB2();
}

void ChargeModule::setDefaultInfoOAB()
{
    flagSetDefaultInfoOAB = true;

    message.setParametrs(adrCharOAB,1007,0,voltageOAB);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharOAB,1008,0,currentOAB);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharOAB,1009,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharOAB1,1007,0,voltageOAB);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharOAB1,1008,0,currentOAB);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharOAB1,1009,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    MainPostMan::setDefaultInfoOAB();
}

void ChargeModule::setDefaultInfoVAB1()
{
    flagSetDefaultInfoVAB1 = true;

    message.setParametrs(adrCharVAB1,1007,0,voltageVAB1);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharVAB1,1008,0,currentVAB1);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharVAB1,1009,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    MainPostMan::setDefaultInfoVAB1();
}

void ChargeModule::setDefaultInfoVAB2()
{
    flagSetDefaultInfoVAB2 = true;

    message.setParametrs(adrCharVAB2,1007,0,voltageVAB2);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharVAB2,1008,0,currentVAB2);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    message.setParametrs(adrCharVAB2,1009,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    MainPostMan::setDefaultInfoVAB2();
}

void ChargeModule::startOAB()
{
    flagStartOAB = true;

    message.setParametrs(adrCharOAB,1009,0,3);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    message.setParametrs(adrCharOAB1,1009,0,3);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startOAB();
}

void ChargeModule::startVAB1()
{
    flagStartVAB1 = true;

    message.setParametrs(adrCharVAB1,1009,0,3);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startVAB1();

}

void ChargeModule::startVAB2()
{
    flagStartVAB2 = true;

    message.setParametrs(adrCharVAB2,1009,0,3);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startVAB2();
}

void ChargeModule::stopOAB()
{
    flagStartOAB = true;

    message.setParametrs(adrCharOAB,1009,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    message.setParametrs(adrCharOAB1,1009,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::startOAB();
}

void ChargeModule::stopVAB1()
{
    flagStopVAB1 = true;

    message.setParametrs(adrCharVAB1,1009,0,0);
    ModbusDevice->setRegisters(message);
    QThread::msleep(10);

    MainPostMan::stopVAB1();
}

void ChargeModule::stopVAB2()
{
    flagStopVAB2 = true;

    message.setParametrs(adrCharVAB2,1009,0,0);
    ModbusDevice->setRegisters(message);

    MainPostMan::stopVAB2();
}

void ChargeModule::setVltOAB(int value)
{
    flagSetVltOAB = true;

    message.setParametrs(adrCharOAB,1007,0,value);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    MainPostMan::setVltOAB(0);
}

void ChargeModule::setVltVAB1(int value)
{
    flagSetVltVAB1 = true;

    message.setParametrs(adrCharVAB1,1007,0,value);
    ModbusDevice->setRegisters(message);
    QThread::msleep(30);

    MainPostMan::setVltVAB1(0);
}

void ChargeModule::setVltVAB2(int value)
{
    flagSetVltVAB2 = true;

     message.setParametrs(adrCharVAB2,1007,0,value);
     ModbusDevice->setRegisters(message);
     QThread::msleep(30);

    MainPostMan::setVltVAB2(0);
}

void ChargeModule::updatePresets(int index)
{
    QFile configFile (QCoreApplication::applicationDirPath() +
                      PATH_JSON +
                      PRESETES_FILE);
    QJsonDocument jsonDoc;  
    QStringList workMode;

        workMode.append("LiOnChargeBalanceAll");
        workMode.append("LiOnChargeNoBalanceAll");
        workMode.append("LiOnDischarge");
        workMode.append("LiOnChargeBalanceModule");
        workMode.append("LiOnChargeNoBalanceModule");
        workMode.append("PbACChargeBalanceAll");
        workMode.append("PbACChargeNoBalanceAll");
        workMode.append("PbACDischarge");
        workMode.append("PbACChargeBalanceModule");
        workMode.append("PbACChargeNoBalanceModule");
        workMode.append("VoltageSource");
        workMode.append("ResetPreset");

    if(configFile.exists())
    {
        configFile.open(QFile::ReadOnly);

        jsonDoc = QJsonDocument().fromJson(configFile.readAll());
        QJsonObject obj =jsonDoc.object();

        QJsonValue jsonValue = obj.value(workMode[index]);

        if(jsonValue.isArray()){
            QJsonArray jsonArray = jsonValue.toArray();
            for (int var = 0; var < jsonArray.count(); ++var)
            {
                QJsonObject subtree = jsonArray.at(var).toObject();

                voltageOAB  = subtree.value("voltageOAB").toInt();
                currentOAB  = subtree.value("currentOAB").toInt();
                voltageVAB1 = subtree.value("voltageVAB1").toInt();
                currentVAB1 = subtree.value("currentVAB1").toInt();
                voltageVAB2 = subtree.value("voltageVAB2").toInt();
                currentVAB2 = subtree.value("currentVAB2").toInt();
                balanceFlag = subtree.value("balanceFlag").toInt();
            }

        configFile.close();
        }

        QList<int> vec;
        vec.append(voltageOAB);
        vec.append(currentOAB);
        vec.append(voltageVAB1);
        vec.append(currentVAB1);
        vec.append(voltageVAB2);
        vec.append(currentVAB2);
        vec.append(balanceFlag);
        qDebug() << "Загруженные настройки - " << vec;
    }
    else
    {
        QList<int> vec;
        vec.append(voltageOAB);
        vec.append(currentOAB);
        vec.append(voltageVAB1);
        vec.append(currentVAB1);
        vec.append(voltageVAB2);
        vec.append(currentVAB2);
        vec.append(balanceFlag);
        for (int var = 0; var < vec.count(); ++var) {
           vec[var] = 0;
        }

    }

    setDefaultInfoOAB();
    setDefaultInfoVAB1();
    setDefaultInfoVAB2();

    MainPostMan::updatePresets(0);
}
