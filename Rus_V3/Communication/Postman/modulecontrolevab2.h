#ifndef MODULECONTROLEVAB2_H
#define MODULECONTROLEVAB2_H

#include "mainpostman.h"

class moduleControleVAB2 : public MainPostMan
{
public:
    moduleControleVAB2(QString = "");

    QTimer *timer;

public slots:
    void communicationWithDevices();
    void start();
    void stop();

    void exchangeCycle();

    void getMcInfoVAB2();

   void getBattMC();

   void startDischargeVAB2();
   void stopDischargeVAB2();
   void startBalanceVAB2();
   void stopBalanceVAB2();

   void updatePresets(int index);


private:
    QString jsonPortName;

    bool flagExchangeCycle;

    bool flagGetMcInfoVAB2;

    bool flagGetBattMc;

    bool flagStartDischargeVAB2;
    bool flagStopDischargeVAB2;
    bool flagStartBalanceVAB2;
    bool flagStopBalanceVAB2;

    int delay;
};

#endif // MODULECONTROLEVAB2_H
