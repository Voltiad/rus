#ifndef CHARGEMODULE_H
#define CHARGEMODULE_H

#include <QString>


#include "RusNamespace.h"
#include "mainpostman.h"

class ChargeModule : public MainPostMan
{
public:
    ChargeModule(QString = "");

    QTimer *timer;

public slots:
    void communicationWithDevices();
    void start();
    void stop();

    void exchangeCycle();

    void getDefaultInfoOAB();
    void infoOAB();
    void infoOAB1();
    void getDefaultInfoVAB1();
    void getDefaultInfoVAB2();

    void setDefaultInfoOAB();
    void setDefaultInfoVAB1();
    void setDefaultInfoVAB2();

    void startOAB();
    void startVAB1();
    void startVAB2();

    void stopOAB();
    void stopVAB1();
    void stopVAB2();

    void setVltOAB(int value);
    void setVltVAB1(int value);
    void setVltVAB2(int value);

    void updatePresets(int index);
private:
    int voltageOAB;
    int currentOAB;
    int voltageVAB1;
    int currentVAB1;
    int voltageVAB2;
    int currentVAB2;
    int balanceFlag;

private:
    QString jsonPortName;

    bool flagExchangeCycle;

    bool flagGetDefaultInfoOAB;
    bool flagGetDefaultInfoVAB1;
    bool flagGetDefaultInfoVAB2;

    bool flagSetDefaultInfoOAB;
    bool flagSetDefaultInfoVAB1;
    bool flagSetDefaultInfoVAB2;

    bool flagStartOAB;
    bool flagStartVAB1;
    bool flagStartVAB2;

    bool flagStopOAB;
    bool flagStopVAB1;
    bool flagStopVAB2;

    bool flagSetVltOAB;
    bool flagSetVltVAB1;
    bool flagSetVltVAB2;



    int delay;


};

#endif // CHARGEMODULE_H
