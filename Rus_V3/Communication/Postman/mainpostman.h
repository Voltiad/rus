#ifndef MAINPOSTMAN_H
#define MAINPOSTMAN_H

#include <QObject>
#include <QVector>
#include <QEventLoop>
#include <QTime>
#include <QTimer>
#include <QDebug>
#include <QCoreApplication>
#include <QMutex>

#include <datastore.h>
#include "modbussettings.h"


class MainPostMan : public QObject
{
    Q_OBJECT
public:
    explicit MainPostMan(QObject *parent = 0);
    ModbusSettings *ModbusDevice;
    QVector<short> receiveMessage;

public:
    int msectmr;
    virtual void setInterval(int msec);
    virtual void start(){}
    virtual void stop() {}
    int numMode;
    void setNumMode(int num){numMode = num;}
    int getNumMode(){return numMode;}

public slots:
    void loop();
public:
    virtual void communicationWithDevices(){}

public:
    inline void setPortName(QString);
    void sleep(qint64 msec);
    Message message;
    QTime *timeElapsed;

private:
    QEventLoop eventLoop;
    QString portName;

public slots:
    virtual void exchangeCycle(){}


public:
    virtual void getMcInfoOAB(){}
    virtual void getMcInfoVAB1(){}
    virtual void getBattMC(){}

    virtual void startDischargeOAB(){}
    virtual void startDischargeVAB1(){}

    virtual void stopDischargeOAB(){}
    virtual void stopDischargeVAB1(){}

    virtual void startBalanceOAB(){}
    virtual void startBalanceVAB1(){}

    virtual void stopBalanceOAB(){}
    virtual void stopBalanceVAB1(){}

    int tryMcOAB;
    int cntMcOAB;

    int tryMcVAB1;
    int cntMcVAB1;


public:
    virtual void getDefaultInfoOAB(){}
    virtual void getDefaultInfoVAB1(){}
    virtual void getDefaultInfoVAB2(){}

    virtual void setDefaultInfoOAB(){}
    virtual void setDefaultInfoVAB1(){}
    virtual void setDefaultInfoVAB2(){}

    virtual void startOAB(){}
    virtual void startVAB1(){}
    virtual void startVAB2(){}

    virtual void stopOAB(){}
    virtual void stopVAB1(){}
    virtual void stopVAB2(){}

    virtual void setVltOAB(int value){}
    virtual void setVltVAB1(int value){}
    virtual void setVltVAB2(int value){}

    virtual void updatePresets(int index){}

signals:
    void connetChargeOAB();
    void disconnectChargeOAB();
    void connetChargeVAB1();
    void disconnectChargeVAB1();
    void connetChargeVAB2();
    void disconnectChargeVAB2();

public:
    int tryChargeOAB;
    int cntChargeOAB;
    bool cntOAB;

    int tryChargeOAB1;
    int cntChargeOAB1;
    bool cntOAB1;

    int tryChargeVAB1;
    int cntChargeVAB1;

    int tryChargeVAB2;
    int cntChargeVAB2;

public:
    virtual void getDefBMS(){}

    virtual void getDefBatt(){}

        virtual void startBalanceBMS(){}

        virtual void stopBalanceBMS(){}

    int tryBms;
    int cntBms;

public:
    virtual void getMcInfoVAB2(){}

    virtual void startDischargeVAB2(){}

    virtual void stopDischargeVAB2(){}

    virtual void startBalanceVAB2(){}

    virtual void stopBalanceVAB2(){}

    int tryMcVAB2;
    int cntMcVAB2;
public:
    QList<int> rspOAB;
    QList<int> rspOAB2;
    QList<int> rspVAB1;
    QList<int> rspVAB2;

    QList<int> rspMcOAB;
    QList<int> rspMcVAB1;
    QList<int> rspMcVAB2;
    QList<int> rspBatt;

    QList<int> rspBMS;
    QList<int> rspBattBms;

public:
    dsChargeModule *chargerOAB;
    dsChargeModule *chargerVAB1;
    dsChargeModule *chargerVAB2;

    dsModuleControle *moduleCOAB;
    dsModuleControle *moduleCVAB1;
    dsModuleControle *moduleCVAB2;
    dsBattery *moduleControleBattery;

    dsBattManagSystem *battManagSystem;
    dsBattery *bmsBattery;

signals:
    void reqOAB();
    void reqOAB2();
    void reqVAB1();
    void reqVAB2();

    void reqMcOAB();
    void reqMcVAB1();
    void reqMcVAB2();

    void reqBMS();
    void reqBatt1();
    void reqBatt2();
    void reqBatt3();
    void reqBatt4();
    void reqBatt5();
    void reqBatt6();
};

inline void MainPostMan::setPortName(QString nameJson)
{
    portName = nameJson;
    ModbusDevice->createRtuModbusConnection(portName);
}


#endif // MAINPOSTMAN_H
