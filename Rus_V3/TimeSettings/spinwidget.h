#ifndef SPINWIDGET_H
#define SPINWIDGET_H

#include <QWidget>

namespace Ui {
class SpinWidget;
}

class SpinWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SpinWidget(QWidget *parent = 0);
    ~SpinWidget();

    void setMaximumValue(int);
    void setMinimunValue(int);
    void setValue(int);
    int value() const;

public slots:
    void valueChangeUp();
    void valueChangeDown();
    void updateForm();


private:
    int currentValue;
    int maximumValue;
    int minimumValue;
    Ui::SpinWidget *ui;

signals:
    void ButtonClick();
};

inline void SpinWidget::setMaximumValue(int inputValue){
    maximumValue = inputValue;
}

inline void SpinWidget::setMinimunValue(int inputValue){
    minimumValue = inputValue;
}

inline void SpinWidget::setValue(int inputValue){
    currentValue = inputValue;
}

#endif // SPINWIDGET_H
