#ifndef TIMESETTINGS_H
#define TIMESETTINGS_H

#include <QWidget>

#include <QGraphicsDropShadowEffect>
#include <QGraphicsEffect>

namespace Ui {
class TimeSettings;
}

class TimeSettings : public QWidget
{
    Q_OBJECT

public:
    explicit TimeSettings(QWidget *parent = 0);
    ~TimeSettings();

public slots:
    void reloadData();

private slots:
    void on_btnApply_clicked();

    void formCorrection();
    void updateWidget();
    void on_btnCancel_clicked();

signals:
    void closeTimeSettings();

private:
    Ui::TimeSettings *ui;

    QTime *time;
    QDate *date;

};
#endif // TIMESETTINGS_H
