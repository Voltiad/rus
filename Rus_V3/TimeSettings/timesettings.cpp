#include <QProcess>
#include <QTime>
#include <QDate>
#include <QLineEdit>

#include "timesettings.h"
#include "ui_timesettings.h"

TimeSettings::TimeSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimeSettings)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::CustomizeWindowHint);

    connect(ui->year,&SpinWidget::ButtonClick,
            this,&TimeSettings::formCorrection);
    connect(ui->month,&SpinWidget::ButtonClick,
            this,&TimeSettings::formCorrection);

    QGraphicsDropShadowEffect *ShadowBtn;
    ShadowBtn = new QGraphicsDropShadowEffect;
    ShadowBtn->setBlurRadius(9.0);
    ShadowBtn->setColor(QColor(0, 0, 0, 160));
    ShadowBtn->setOffset(4.0);
    ui->btnApply->setGraphicsEffect(ShadowBtn);

    QGraphicsDropShadowEffect *ShadowBtn2;
    ShadowBtn2 = new QGraphicsDropShadowEffect;
    ShadowBtn2->setBlurRadius(9.0);
    ShadowBtn2->setColor(QColor(0, 0, 0, 160));
    ShadowBtn2->setOffset(4.0);
    ui->btnCancel->setGraphicsEffect(ShadowBtn2);

    time = new QTime();
    date = new QDate();

    ui->days->setMaximumValue(31);
    ui->month->setMaximumValue(12);
    ui->year->setMaximumValue(2100);
    ui->year->setMinimunValue(1970);
    ui->year->setValue(2018);

    ui->hour->setMaximumValue(23);
    ui->hour->setMinimunValue(0);
    ui->min->setMaximumValue(59);
    ui->sec->setMaximumValue(59);
    ui->min->setMinimunValue(0);
    ui->sec->setMinimunValue(0);
}

TimeSettings::~TimeSettings()
{
    delete ui;
}

void TimeSettings::on_btnApply_clicked()
{
   QProcess *proc = new QProcess();
   QString monthNum;
   switch (ui->month->value())
   {
   case 1:monthNum = "Jan";
       break;
   case 2:monthNum = "Feb";
       break;
   case 3:monthNum = "Mar";
       break;
   case 4:monthNum = "Apr";
       break;
   case 5:monthNum = "May";
       break;
   case 6:monthNum = "Jun";
       break;
   case 7:monthNum = "Jul";
       break;
   case 8:monthNum = "Aug";
       break;
   case 9:monthNum = "Sep";
       break;
   case 10:monthNum = "Oct";
       break;
   case 11:monthNum = "Nov";
       break;
   case 12:monthNum = "Dec";
       break;
   default:
       break;
   }

   QString Date_Time = "sudo date -s \""
            + QString::number(ui->days->value()) + " "
            + monthNum + " "
            + QString::number(ui->year->value()) + " "
            + QString::number(ui->hour->value()) + ":"
            + QString::number(ui->min->value())+ ":"
            + QString::number(ui->sec->value());

   proc->start(Date_Time);

   emit closeTimeSettings();
}

void TimeSettings::reloadData()
{

    ui->hour->setValue(time->currentTime().hour());
    ui->min->setValue(time->currentTime().minute());
    ui->sec->setValue(time->currentTime().second());

    ui->days->setValue(date->currentDate().day());
    ui->month->setValue(date->currentDate().month());
    ui->year->setValue(date->currentDate().year());

    formCorrection();
}

void TimeSettings::formCorrection()
{
    switch (ui->month->value()) {
    case 1:
        ui->days->setMaximumValue(31);
        break;
    case 2:
        if(ui->year->value()%4!=0){
            ui->days->setMaximumValue(28);
        }else{
            ui->days->setMaximumValue(29);
        }
        break;
    case 3:
        ui->days->setMaximumValue(31);
        break;
    case 4:
        ui->days->setMaximumValue(30);
        break;
    case 5:
        ui->days->setMaximumValue(31);
        break;
    case 6:
        ui->days->setMaximumValue(30);
        break;
    case 7:
        ui->days->setMaximumValue(31);
        break;
    case 8:
        ui->days->setMaximumValue(31);
        break;
    case 9:
        ui->days->setMaximumValue(30);
        break;
    case 10:
        ui->days->setMaximumValue(31);
        break;
    case 11:
        ui->days->setMaximumValue(30);
        break;
    case 12:
        ui->days->setMaximumValue(31);
        break;
    default:
        break;
    }

    updateWidget();
}

void TimeSettings::updateWidget()
{
    ui->days->updateForm();
    ui->month->updateForm();
    ui->year->updateForm();
    ui->hour->updateForm();
    ui->min->updateForm();
    ui->sec->updateForm();
}

void TimeSettings::on_btnCancel_clicked()
{
   emit closeTimeSettings();
}
