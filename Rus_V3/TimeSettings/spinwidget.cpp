 #include "spinwidget.h"
#include "ui_spinwidget.h"
#include <QGraphicsDropShadowEffect>
#include <QPropertyAnimation>

SpinWidget::SpinWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpinWidget)
{
    ui->setupUi(this);

    currentValue = 1;

    maximumValue = 24;
    minimumValue = 1;

    connect(ui->btnUP,&QPushButton::clicked,
            this,&SpinWidget::valueChangeUp);

    connect(ui->btnDOWN,&QPushButton::clicked,
            this,&SpinWidget::valueChangeDown);

    QGraphicsDropShadowEffect *ShadowBtn;
    ShadowBtn = new QGraphicsDropShadowEffect;
    ShadowBtn->setBlurRadius(9.0);
    ShadowBtn->setColor(QColor(0, 0, 0, 160));
    ShadowBtn->setOffset(4.0);
    ui->btnUP->setGraphicsEffect(ShadowBtn);

    QGraphicsDropShadowEffect *ShadowBtn2;
    ShadowBtn2 = new QGraphicsDropShadowEffect;
    ShadowBtn2->setBlurRadius(9.0);
    ShadowBtn2->setColor(QColor(0, 0, 0, 160));
    ShadowBtn2->setOffset(4.0);
    ui->btnDOWN->setGraphicsEffect(ShadowBtn2);

}

SpinWidget::~SpinWidget()
{
    delete ui;
}

int SpinWidget::value() const{
    return currentValue;
}

void SpinWidget::valueChangeUp()
{
    currentValue ++;
    emit ButtonClick();
    updateForm();


//    QPropertyAnimation *centralUp = new QPropertyAnimation(ui->centralValue,"geometry");
//    centralUp->setDuration(200);
//    centralUp->setEasingCurve(QEasingCurve::OutCurve);
//    centralUp->setEndValue(QRect(45,45,85,38));
//    centralUp->start(QAbstractAnimation::DeleteWhenStopped);

}

void SpinWidget::valueChangeDown()
{
    currentValue--;
    emit ButtonClick();
    updateForm();
}

void SpinWidget::updateForm()
{
    if(currentValue > maximumValue)
    {
        currentValue = minimumValue;
    }
    if(currentValue < minimumValue)
    {
        currentValue = maximumValue;
    }

    if(currentValue < 10)
    {
        ui->centralValue->setText("0"+QString::number(currentValue,'i',0));
    } else {
        ui->centralValue->setText(QString::number(currentValue,'i',0));
    }


    int min = currentValue -1;
    int max = currentValue +1;

    if(min<minimumValue) min = maximumValue;
    if(max>maximumValue) max = minimumValue;

    if(max < 10)
    {
        ui->upValue->setText("0"+QString::number(max,'i',0));
    } else {
        ui->upValue->setText(QString::number(max,'i',0));
    }
    if(min < 10)
    {
        ui->downValue->setText("0"+QString::number(min,'i',0));
    } else {
        ui->downValue->setText(QString::number(min,'i',0));
    }

    int minDown = min-1;
    int maxUp = max+1;

    if(minDown<minimumValue) minDown = maximumValue;
    if(maxUp>maximumValue) maxUp = minimumValue;

    if(maxUp < 10)
    {
        ui->upUpValue->setText("0"+QString::number(maxUp,'i',0));
    } else {
        ui->upUpValue->setText(QString::number(maxUp,'i',0));
    }
    if(minDown < 10)
    {
        ui->downDownValue->setText("0"+QString::number(minDown,'i',0));
    } else {
        ui->downDownValue->setText(QString::number(minDown,'i',0));
    }

}
