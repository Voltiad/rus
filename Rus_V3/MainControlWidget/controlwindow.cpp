#include "controlwindow.h"
#include "ui_controlwindow.h"

#include <QDebug>

ControlWindow::ControlWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlWindow)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::CustomizeWindowHint);

    ui->widgetOAB->setHeaderModule("ОАБ");
    ui->widgetVAB1->setHeaderModule("ВАБ 1");
    ui->widgetVAB2->setHeaderModule("ВАБ 2");

    connect(ui->widgetOAB,&ControlModuleWidget::powerButton,
            [this](bool checked){
        if(checked == true){
           emit powerOnOAB();
        }else{
           emit powerOffOAB();
        }
        });

    connect(ui->widgetVAB1,&ControlModuleWidget::powerButton,
            [this](bool checked){
        if(checked == true){
           emit powerOnVAB1();
        }else{
           emit powerOffVAB1();
        }
        });

    connect(ui->widgetVAB2,&ControlModuleWidget::powerButton,
            [this](bool checked){
        if(checked == true){
           emit powerOnVAB2();
        }else{
           emit powerOffVAB2();
        }
        });

    connect(ui->widgetOAB,&ControlModuleWidget::updateVoltage,
            [this](int value){
        emit updateVoltageOAB(value);
        });

    connect(ui->widgetVAB1,&ControlModuleWidget::updateVoltage,
            [this](int value){
        emit updateVoltageVAB1(value);
        });

    connect(ui->widgetVAB2,&ControlModuleWidget::updateVoltage,
            [this](int value){
        emit updateVoltageVAB2(value);
        });

    connect(ui->widgetOAB,&ControlModuleWidget::startBalance,
            [this](){
        emit startBalanceOAB();
        });

    connect(ui->widgetVAB1,&ControlModuleWidget::startBalance,
            [this](){
        emit startBalanceVAB1();
        });

    connect(ui->widgetVAB2,&ControlModuleWidget::startBalance,
            [this](){
        emit startBalanceVAB2();
        });

    connect(ui->widgetOAB,&ControlModuleWidget::stopBalance,
            [this](){
        emit stopBalanceOAB();
        });

    connect(ui->widgetVAB1,&ControlModuleWidget::stopBalance,
            [this](){
        emit stopBalanceVAB1();
        });

    connect(ui->widgetVAB2,&ControlModuleWidget::stopBalance,
            [this](){
        emit stopBalanceVAB2();
        });

}

ControlWindow::~ControlWindow()
{
    delete ui;
}

void ControlWindow::resetButtons()
{
    ui->widgetOAB->resetButtons();
    ui->widgetVAB1->resetButtons();
    ui->widgetVAB2->resetButtons();
}

void ControlWindow::updateMemoryDatas()
{
    ui->widgetOAB->dsCharger = dsOAB;
    ui->widgetVAB1->dsCharger = dsVAB1;
    ui->widgetVAB2->dsCharger = dsVAB2;

    ui->widgetOAB->dsMoudCont = dsMcOAB;
    ui->widgetVAB1->dsMoudCont = dsMcVAB1;
    ui->widgetVAB2->dsMoudCont = dsMcVAB2;

    ui->widgetOAB->dsBMS = dsBMS;
    ui->widgetVAB1->dsBMS = dsBMS;
    ui->widgetVAB2->dsBMS = dsBMS;

    ui->widgetOAB->dsBmsBattery = dsBmsBattery;
    ui->widgetVAB1->dsBmsBattery = dsBmsBattery;
    ui->widgetVAB2->dsBmsBattery = dsBmsBattery;

    ui->widgetOAB->updateInfoButtonMemory();
    ui->widgetVAB1->updateInfoButtonMemory();
    ui->widgetVAB2->updateInfoButtonMemory();

}
void ControlWindow::updateAlgorithm()
{
    ui->widgetOAB->updateAlg();
    ui->widgetVAB1->updateAlg();
    ui->widgetVAB2->updateAlg();
}

void ControlWindow::setCenterWidget(int num)
{
    ui->widgetOAB->setCentralWidget(num);
    ui->widgetVAB1->setCentralWidget(num);
    ui->widgetVAB2->setCentralWidget(num);

}
