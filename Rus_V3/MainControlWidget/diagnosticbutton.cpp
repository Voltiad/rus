#include "diagnosticbutton.h"
#include "RusNamespace.h"
#include <QDebug>


DiagnosticButton::DiagnosticButton(QWidget *parent) :
    QPushButton(parent)
{
    connect(this,SIGNAL(clicked()),this,SLOT(clickOnbutton()));

    diagWin = new DiagnosticWindow;
    diagWin->setWindowFlags(Qt::FramelessWindowHint |
                            Qt::WindowStaysOnTopHint);

}

void DiagnosticButton::clickOnbutton()
{
    diagWin->updateInfo();
    diagWin->show();
}

void DiagnosticButton::updateMemoryDiagnosticWidget()
{
    diagWin->dsBMS = dsBMS;
    diagWin->dsBmsBattery = dsBmsBattery;
    diagWin->dsCharger = dsCharger;
    diagWin->dsMoudCont = dsMoudCont;

    diagWin->setRangeBatts(minRange,maxRange);

    diagWin->flags = flags;
}

void DiagnosticButton::checkErrors(int num)
{
    setNumMode(num);
    diagWin->setNumMode(getNumMode());
    diagWin->updateInfo();


    if(flags->connectionStatus == false)
    {
        setCustomStyleButton(buttonNormal,noConnectionBMSShort);
        return;
    }
    if(flags->flagErrorsBatt == RS::Emergency){
        setCustomStyleButton(buttonEmergency,emergencyTextShort);
        return;
    }
    if(dsCharger->voltageOne < 500){
        setCustomStyleButton(buttonReady,noConnectionForceCableShort);
    }

    setCustomStyleButton(buttonReady,"Неисправности\nотсутсвуют");
    return;

//    if(flags->flagErrorsBatt&RS::Emergency){
//        setStyleSheet(buttonEmergency);
//        setText("Неисправно");
//        return;
//    }
//    setStyleSheet(buttonNormal);


//    setStyleSheet(buttonReady);
//    setText("Готовность");

}

void DiagnosticButton::setCustomStyleButton(QString css_style, QString textButton)
{
    setStyleSheet(css_style);
    setText(textButton);
}
