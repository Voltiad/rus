#include "capacitive_characteristic.h"

//Исходные значения точек (x = capacity,y = voltage)
Data Point_values[number_of_points] = {
    {0, 3100 },
    {2, 3300 },
    {5, 3410 },
    {11, 3490 },
    {23, 3570 },
    {35, 3650 },
    {47, 3720 },
    {59, 3800 },
    {71, 3880 },
    {83, 3950 },
    {95, 4030 },
    {98, 4150 },
    {100, 4200 }
 };

Data Table[x2/dx];

int InterpolationLangranj (Data f[], int n, float X)
{
    float L,l;
    int i,j;

    L = 0;

    for (i = 0; i < n; ++i)
    {
        l = 1;

        for (j = 0; j < n; ++j)
            if (i != j)
                l *= (X-f[j].x) / (f[i].x-f[j].x);

        L += f[i].y*l;
    }

    return int(L);
}

void InitTableVoltageCapacity()
{
    for (int xi = x1; xi <= x2; xi += dx) {
        Table[xi].x = xi;
        Table[xi].y = InterpolationLangranj(Point_values, number_of_points, xi);
    }

}

int VoltageToCapacity(int u)
{
    if (u < Table[0].y) {
        return Table[0].x;
    }
    if (u > Table[x2/dx].y) {
        return Table[x2/dx].x;
    }
    for (int i = 0; i <= int(x2/dx); i++) {
        if (u < Table[i].y) {
          return Table[i-1].x;
        }
        if (u == Table[i].y) {
            return Table[i].x;
        }
    }
    return -1;
}
