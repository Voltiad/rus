#include "percentwidget.h"
#include "ui_percentwidget.h"
#include <QDebug>

#include "capacitive_characteristic.h"

PerCentWidget::PerCentWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PerCentWidget)
{
    ui->setupUi(this);
    InitTableVoltageCapacity();
}

PerCentWidget::~PerCentWidget()
{
    delete ui;
}

void PerCentWidget::setValues(int currCap,int maxCap)
{
    float percent = (float)currCap/(float)maxCap*100;
    ui->voltge_pc->setText(QString::number(percent,'f',1) + "%");
    ui->progressBar->setValue(percent);
    ui->progressBar->setFormat(QString("%1 А/ч").arg((float)currCap));
}
