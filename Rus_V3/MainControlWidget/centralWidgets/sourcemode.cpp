#include "sourcemode.h"
#include "ui_sourcemode.h"

SourceMode::SourceMode(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SourceMode)
{
    ui->setupUi(this);
}

SourceMode::~SourceMode()
{
    delete ui;
}

void SourceMode::on_horizontalSlider_sliderMoved(int position)
{
    ui->valueVoltage->setText(QString::number((float)position/100,'f',1) + " В");
}
