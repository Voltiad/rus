#ifndef CAPACITIVE_CHARACTERISTIC_H
#define CAPACITIVE_CHARACTERISTIC_H

struct Data { int x, y; };

//Количество исходных точек
static const int number_of_points = 13;

//Tаблицa значений от x1 до x2 с шагом dx
static const int x1 = 0,
                 x2 = 100,
                 dx = 1;

extern Data Point_values[number_of_points];
extern Data Table[x2/dx];

void InitTableVoltageCapacity();
int VoltageToCapacity(int u);

#endif // CAPACITIVE_CHARACTERISTIC_H
