#include "amperwidget.h"
#include "ui_amperwidget.h"

AmperWidget::AmperWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AmperWidget)
{
    ui->setupUi(this);
}

AmperWidget::~AmperWidget()
{
    delete ui;
}
