#ifndef SOURCEMODE_H
#define SOURCEMODE_H

#include <QWidget>

namespace Ui {
class SourceMode;
}

class SourceMode : public QWidget
{
    Q_OBJECT

public:
    explicit SourceMode(QWidget *parent = 0);
    ~SourceMode();

private slots:
    void on_horizontalSlider_sliderMoved(int position);

private:
    Ui::SourceMode *ui;
};

#endif // SOURCEMODE_H
