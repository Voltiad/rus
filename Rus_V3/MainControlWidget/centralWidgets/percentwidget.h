#ifndef PERCENTWIDGET_H
#define PERCENTWIDGET_H

#include <QWidget>

namespace Ui {
class PerCentWidget;
}

class PerCentWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PerCentWidget(QWidget *parent = 0);
    ~PerCentWidget();

private:
    Ui::PerCentWidget *ui;

public:
    void setValues(int currCap, int maxCap);
};

#endif // PERCENTWIDGET_H
