#ifndef AMPERWIDGET_H
#define AMPERWIDGET_H

#include <QWidget>

namespace Ui {
class AmperWidget;
}

class AmperWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AmperWidget(QWidget *parent = 0);
    ~AmperWidget();

private:
    Ui::AmperWidget *ui;
};

#endif // AMPERWIDGET_H
