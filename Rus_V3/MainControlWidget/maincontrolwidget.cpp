#include "maincontrolwidget.h"
#include "ui_maincontrolwidget.h"
#include "capacitive_characteristic.h"
#include <QDebug>

MainControlWidget::MainControlWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainControlWidget)
{
    ui->setupUi(this);

    InitTableVoltageCapacity();

    windowSetting();

    generalSetting();

}

MainControlWidget::~MainControlWidget()
{
    delete ui;
}

void MainControlWidget::windowSetting()
{
    this->setWindowFlags(Qt::CustomizeWindowHint);
}

void MainControlWidget::generalSetting()
{
    settingDateTime();

    settingButtons();

    settingSignalsPowerButtons();
}

void MainControlWidget::lateSetting()
{
    ui->HeadWindow->dsOAB = dsOAB_disp;
    ui->HeadWindow->dsVAB1 = dsVAB1_disp;
    ui->HeadWindow->dsVAB2 = dsVAB2_disp;

    ui->HeadWindow->dsMcOAB = dsMcOAB_disp;
    ui->HeadWindow->dsMcVAB1 = dsMcVAB1_disp;
    ui->HeadWindow->dsMcVAB2 = dsMcVAB2_disp;

    ui->HeadWindow->dsBMS = dsBMS_disp;

    ui->HeadWindow->dsBmsBattery = dsBmsBattery_disp;

    ui->HeadWindow->updateMemoryDatas();

    dsBmsBattery_disp->mutex.lock();

       ui->BattOAB->setValues_am0(&dsBmsBattery_disp->cell[0],
               &dsBmsBattery_disp->cell[1],
               &dsBmsBattery_disp->cell[2],
               &dsBmsBattery_disp->cell[3],
               &dsBmsBattery_disp->cell[4],
               &dsBmsBattery_disp->cell[5],
               &dsBmsBattery_disp->cell[6],
              &dsBmsBattery_disp->cell[7]);

       ui->BattOAB->setValues_am1(&dsBmsBattery_disp->cell[8],
               &dsBmsBattery_disp->cell[9],
               &dsBmsBattery_disp->cell[10],
               &dsBmsBattery_disp->cell[11],
               &dsBmsBattery_disp->cell[12],
               &dsBmsBattery_disp->cell[13],
               &dsBmsBattery_disp->cell[14],
               &dsBmsBattery_disp->cell[15]);

       ui->BattOAB->setValues_am2(&dsBmsBattery_disp->cell[16],
               &dsBmsBattery_disp->cell[17],
               &dsBmsBattery_disp->cell[18],
               &dsBmsBattery_disp->cell[19],
               &dsBmsBattery_disp->cell[20],
               &dsBmsBattery_disp->cell[21],
               &dsBmsBattery_disp->cell[22],
               &dsBmsBattery_disp->cell[23]);

       ui->BattOAB->setValues_am3(&dsBmsBattery_disp->cell[24],
               &dsBmsBattery_disp->cell[25],
               &dsBmsBattery_disp->cell[26],
               &dsBmsBattery_disp->cell[27],
               &dsBmsBattery_disp->cell[28],
               &dsBmsBattery_disp->cell[29],
               &dsBmsBattery_disp->cell[30],
               &dsBmsBattery_disp->cell[31]);

       ui->BattVAB2->setValues(&dsBmsBattery_disp->cell[32],
               &dsBmsBattery_disp->cell[33],
               &dsBmsBattery_disp->cell[34],
               &dsBmsBattery_disp->cell[35],
               &dsBmsBattery_disp->cell[36],
               &dsBmsBattery_disp->cell[37],
               &dsBmsBattery_disp->cell[38],
               &dsBmsBattery_disp->cell[39]);

       ui->BattVAB1->setValues(&dsBmsBattery_disp->cell[40],
               &dsBmsBattery_disp->cell[41],
               &dsBmsBattery_disp->cell[42],
               &dsBmsBattery_disp->cell[43],
               &dsBmsBattery_disp->cell[44],
               &dsBmsBattery_disp->cell[45],
               &dsBmsBattery_disp->cell[46],
               &dsBmsBattery_disp->cell[47]);

       dsBmsBattery_disp->mutex.unlock();
}

void MainControlWidget::updateValuesBatts()
{
    ui->BattOAB->setValues_am0(&dsBmsBattery_disp->cell[0],
            &dsBmsBattery_disp->cell[1],
            &dsBmsBattery_disp->cell[2],
            &dsBmsBattery_disp->cell[3],
            &dsBmsBattery_disp->cell[4],
            &dsBmsBattery_disp->cell[5],
            &dsBmsBattery_disp->cell[6],
           &dsBmsBattery_disp->cell[7]);

    ui->BattOAB->setValues_am1(&dsBmsBattery_disp->cell[8],
            &dsBmsBattery_disp->cell[9],
            &dsBmsBattery_disp->cell[10],
            &dsBmsBattery_disp->cell[11],
            &dsBmsBattery_disp->cell[12],
            &dsBmsBattery_disp->cell[13],
            &dsBmsBattery_disp->cell[14],
            &dsBmsBattery_disp->cell[15]);

    ui->BattOAB->setValues_am2(&dsBmsBattery_disp->cell[16],
            &dsBmsBattery_disp->cell[17],
            &dsBmsBattery_disp->cell[18],
            &dsBmsBattery_disp->cell[19],
            &dsBmsBattery_disp->cell[20],
            &dsBmsBattery_disp->cell[21],
            &dsBmsBattery_disp->cell[22],
            &dsBmsBattery_disp->cell[23]);

    ui->BattOAB->setValues_am3(&dsBmsBattery_disp->cell[24],
            &dsBmsBattery_disp->cell[25],
            &dsBmsBattery_disp->cell[26],
            &dsBmsBattery_disp->cell[27],
            &dsBmsBattery_disp->cell[28],
            &dsBmsBattery_disp->cell[29],
            &dsBmsBattery_disp->cell[30],
            &dsBmsBattery_disp->cell[31]);

    ui->BattVAB2->setValues(&dsBmsBattery_disp->cell[32],
            &dsBmsBattery_disp->cell[33],
            &dsBmsBattery_disp->cell[34],
            &dsBmsBattery_disp->cell[35],
            &dsBmsBattery_disp->cell[36],
            &dsBmsBattery_disp->cell[37],
            &dsBmsBattery_disp->cell[38],
            &dsBmsBattery_disp->cell[39]);

    ui->BattVAB1->setValues(&dsBmsBattery_disp->cell[40],
            &dsBmsBattery_disp->cell[41],
            &dsBmsBattery_disp->cell[42],
            &dsBmsBattery_disp->cell[43],
            &dsBmsBattery_disp->cell[44],
            &dsBmsBattery_disp->cell[45],
            &dsBmsBattery_disp->cell[46],
            &dsBmsBattery_disp->cell[47]);
}

void MainControlWidget::resetButtons()
{
    ui->HeadWindow->resetButtons();
}

void MainControlWidget::timerEvent(QTimerEvent *event)
{
    int eventID = event->timerId();

    if(eventID == timerDateTime)
    {
        ui->time->setText(
                    QTime::currentTime().toString("hh:mm:ss"));
        ui->date->setText(
                    QDate::currentDate().toString("dd.MM.yy"));
    }

    if(eventID == updateDisplay)
    {
        ui->HeadWindow->updateAlgorithm();
        updateValuesBatts();
    }
}


void MainControlWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_F1:{
            this->hide();
            emit goToWorkMode();
        }
            break;

        case Qt::Key_F2:{
            ui->stackedWidget->setCurrentIndex(0);
            mainButtonGroup->button(0)->click();
        }
            break;

        case Qt::Key_F3:{
            ui->stackedWidget->setCurrentIndex(1);
            mainButtonGroup->button(1)->click();
        }
            break;

        case Qt::Key_F4:{
            ui->stackedWidget->setCurrentIndex(2);
            mainButtonGroup->button(2)->click();
        }
            break;

        case Qt::Key_F5:{
            ui->stackedWidget->setCurrentIndex(3);
            mainButtonGroup->button(3)->click();
        }
            break;

        case Qt::Key_Enter:{
            emit closeProg();
        }
        break;

        default:
            break;
    }
}

void MainControlWidget::settingDateTime(){ timerDateTime = startTimer(1000);}

void MainControlWidget::startDisplayTimer(){ updateDisplay = startTimer(1000);}

void MainControlWidget::stopDisplayTimer(){ updateDisplay = 0;}

void MainControlWidget::setCentralWidget(int numMode)
{
   setNumMode(numMode);

   mainButtonGroup->button(0)->click();

   ui->HeadWindow->setCenterWidget(getNumMode());
}

void MainControlWidget::settingButtons()
{
    mainButtonGroup = new QButtonGroup();

    mainButtonGroup->addButton(ui->F2,0);
    mainButtonGroup->addButton(ui->F3,1);
    mainButtonGroup->addButton(ui->F4,2);
    mainButtonGroup->addButton(ui->F5,3);

    connect(mainButtonGroup,SIGNAL(buttonClicked(int)),
            this,SLOT(buttonClickDisplay(int)));
    connect(ui->F1,SIGNAL(clicked()),
            this,SLOT(clickOnButtonReturn()));
}


void MainControlWidget::buttonClickDisplay(int index)
{
    switch (index)
    {
        case 0:{
            ui->stackedWidget->setCurrentIndex(0);
        }
            break;

        case 1:{
            ui->stackedWidget->setCurrentIndex(1);
        }
            break;

        case 2:{
            ui->stackedWidget->setCurrentIndex(2);
        }
            break;

        case 3:{
            ui->stackedWidget->setCurrentIndex(3);
        }
            break;

        default:
            break;
    }
}

void MainControlWidget::clickOnButtonReturn()
{
    this->hide();
    emit goToWorkMode();
}

void MainControlWidget::setHeaderText(QString text){
    ui->header->setText("Текущий режим работы: " + text);}

void MainControlWidget::updateWorkTimeLbl(int currentWorkTime)
{
    unsigned int min = currentWorkTime/60%60;
    unsigned int hours = currentWorkTime/60/60;

    if(currentWorkTime<60){
        ui->worktime->setText(QString::number(currentWorkTime) + " сек.");
    }else{
        ui->worktime->setText(QString::number(hours) + " ч. " +
                              QString::number(min) + " м.");
    }
}

void MainControlWidget::settingSignalsPowerButtons()
{
    connect(ui->HeadWindow,&ControlWindow::powerOnOAB,
            [this](){ powerBtnOAB = true;
        emit powerOnOAB();});

    connect(ui->HeadWindow,&ControlWindow::powerOnVAB1,
            [this](){powerBtnVAB1 = true;
        emit powerOnVAB1();});

    connect(ui->HeadWindow,&ControlWindow::powerOnVAB2,
            [this](){powerBtnVAB2 = true;
        emit powerOnVAB2();});

    connect(ui->HeadWindow,&ControlWindow::powerOffOAB,
            [this](){powerBtnOAB = false;
        emit powerOffOAB();});

    connect(ui->HeadWindow,&ControlWindow::powerOffVAB1,
            [this](){powerBtnVAB1 = false;
        emit powerOffVAB1();});

    connect(ui->HeadWindow,&ControlWindow::powerOffVAB2,
            [this](){powerBtnVAB2 = false;
        emit powerOffVAB2();});







    connect(ui->HeadWindow,&ControlWindow::updateVoltageOAB,
            [this](int value){
        updateVoltageOAB(value);
        });

    connect(ui->HeadWindow,&ControlWindow::updateVoltageVAB1,
            [this](int value){
        updateVoltageVAB1(value);
        });

    connect(ui->HeadWindow,&ControlWindow::updateVoltageVAB2,
            [this](int value){
        updateVoltageVAB2(value);
        });



    connect(ui->HeadWindow,&ControlWindow::startBalanceOAB,
            [this](){
        emit startBalanceOAB();
        });

    connect(ui->HeadWindow,&ControlWindow::startBalanceVAB1,
            [this](){
        emit startBalanceVAB1();
        });

    connect(ui->HeadWindow,&ControlWindow::startBalanceVAB2,
            [this](){
        emit startBalanceVAB2();
        });

    connect(ui->HeadWindow,&ControlWindow::stopBalanceOAB,
            [this](){
        emit stopBalanceOAB();
        });

    connect(ui->HeadWindow,&ControlWindow::stopBalanceVAB1,
            [this](){
        emit stopBalanceVAB1();
        });

    connect(ui->HeadWindow,&ControlWindow::stopBalanceVAB2,
            [this](){
        emit stopBalanceVAB2();
        });



}

