#ifndef CONTROLMODULEWIDGET_H
#define CONTROLMODULEWIDGET_H

#include <QWidget>
#include <RusNamespace.h>
#include <percentwidget.h>
#include "capacitive_characteristic.h"
#include <datastore.h>
#include <QTime>
#include <QSlider>

namespace Ui {
class ControlModuleWidget;
}

class ControlModuleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ControlModuleWidget(QWidget *parent = 0);
    ~ControlModuleWidget();

public:
    dsChargeModule *dsCharger;
    dsModuleControle *dsMoudCont;
    dsBattManagSystem *dsBMS;
    dsBattery *dsBmsBattery;

    dsFlagsDiagnostic *flags;

    void updateInfoButtonMemory();

public:
    void setDisable();
    void setEnabled();
    void resetButtons();

public:
    void setCentralWidget(int num);
    void setTextButton(int num);
    void setHeaderModule(QString text);
    QString nameModule;
    void setHeaderCentralWidget(QString text);
    int numMode;

private slots:
    void on_powerButton_clicked(bool checked);
    void on_powerButton_clicked();

    void on_infoButton_clicked();
    bool dialogSetting();
    void settingTextButtons();
    void showOneBtnDialog(int capp,int time);

signals:
    void powerButton(bool checked);

public:
    Ui::ControlModuleWidget *ui;

private:
    PerCentWidget *percentWiget;

public:
    void setDisplayValue(int voltage, int current);
    void setCentralWidgetValuePerCent(int currCap, int maxCap);

public:
    void updateAlg();

private:
    void algLiOnChargeModuleWhitoutBalance();
    void algLiOnChargeModuleWhitBalance();
    void algLiOnChargeBattWithoutBalance();
    void algLiOnChargeBattWithBalance();
    void algLiOnDischrage();

    void checkMaxBattAndCorrectionVoltage();
    void chargeDownCurrent();
    bool chargeOver;
    void correctionVoltage(int value);

private:
    int elapseTime = 0;
    float currentCurrent(int curr_one, int curr_two){
       return (float)curr_one/100 + (float)curr_two/100;
    }

    bool checkConectionChargeModule();
    bool checkConectionChargeBMS();
    bool checkInfoDevice(dsBattManagSystem *);
    bool checkInfoDevice(dsModuleControle *);
    bool checkConnectionCharger();
    void checkValid();
    bool checkValidBatt(int minAdr,int maxAdr);
    bool validBatt;

    void checkFlags();
    bool workStatus;
    bool checkFlagsBatt(int minAdr,int maxAdr);

    void findLowAndHighBatt();
    int finfLowVlt(int minAdr,int maxAdr);
    int finfHighVlt(int minAdr,int maxAdr);
    int findSummBatt(int minAdr,int maxAdr);
    bool finalStageFlag;

    int lowBattVlt = 0;
    int highBattVlt = 0;
    int sumBatt;

    void updateValues();
    void updateCentralValue();
    void updtaePercentWidget(int value);
    float currCap = 0;

    void updateLabelWorkTime();


    void testAlg();
    float ampersLoad = 0;
    int workTime = 0;

private:
    QTime *time;

signals:
    void updateVoltage(int value);
    void startBalance();
    void stopBalance();




};

#endif // CONTROLMODULEWIDGET_H
