#ifndef CONTROLWINDOW_H
#define CONTROLWINDOW_H

#include <QWidget>
#include <RusNamespace.h>
#include <datastore.h>

namespace Ui {
class ControlWindow;
}

class ControlWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ControlWindow(QWidget *parent = 0);
    ~ControlWindow();

public:
    dsChargeModule *dsOAB;
    dsChargeModule *dsVAB1;
    dsChargeModule *dsVAB2;

    dsModuleControle *dsMcOAB;
    dsModuleControle *dsMcVAB1;
    dsModuleControle *dsMcVAB2;

    dsBattManagSystem *dsBMS;
    dsBattery *dsBmsBattery;

    void resetButtons();

public:
    void updateMemoryDatas();

public:
    void updateAlgorithm();

public:
    void setCenterWidget(int num);

signals:
    void goToWotkModeWindow();

private:
    Ui::ControlWindow *ui;

signals:
    void powerOnOAB();
    void powerOffOAB();

    void powerOnVAB1();
    void powerOffVAB1();

    void powerOnVAB2();
    void powerOffVAB2();

    void updateVoltageOAB(int value);
    void updateVoltageVAB1(int value);
    void updateVoltageVAB2(int value);

    void startBalanceOAB();
    void stopBalanceOAB();

    void startBalanceVAB1();
    void stopBalanceVAB1();

    void startBalanceVAB2();
    void stopBalanceVAB2();
};

#endif // CONTROLWINDOW_H
