#ifndef CELLVAB2_H
#define CELLVAB2_H

#include <QWidget>
#include <datastore.h>

namespace Ui {
class CellVAB2;
}

class CellVAB2 : public QWidget
{
    Q_OBJECT

public:
    explicit CellVAB2(QWidget *parent = 0);
    ~CellVAB2();

private:
    Ui::CellVAB2 *ui;

public:
    void setValues(dsBatteryCell*cell0,
                   dsBatteryCell*cell1,
                   dsBatteryCell*cell2,
                   dsBatteryCell*cell3,
                   dsBatteryCell*cell4,
                   dsBatteryCell*cell5,
                   dsBatteryCell*cell6,
                   dsBatteryCell*cell7);
};

#endif // CELLVAB2_H
