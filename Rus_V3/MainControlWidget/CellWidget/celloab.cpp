#include "celloab.h"
#include "ui_celloab.h"

CellOAB::CellOAB(QWidget *parent) :
    QWidget(parent),
     ui(new Ui::CellOAB)
{
    ui->setupUi(this);
}

CellOAB::~CellOAB()
{
    delete ui;
}

void CellOAB::setValues_am0(dsBatteryCell *cell0, dsBatteryCell *cell1, dsBatteryCell *cell2, dsBatteryCell *cell3, dsBatteryCell *cell4, dsBatteryCell *cell5, dsBatteryCell *cell6, dsBatteryCell *cell7)
{
    ui->Am0->setValues(cell0,
                       cell1,
                       cell2,
                       cell3,
                       cell4,
                       cell5,
                       cell6,
                       cell7);
}

void CellOAB::setValues_am1(dsBatteryCell *cell0, dsBatteryCell *cell1, dsBatteryCell *cell2, dsBatteryCell *cell3, dsBatteryCell *cell4, dsBatteryCell *cell5, dsBatteryCell *cell6, dsBatteryCell *cell7)
{
    ui->Am1->setValues(cell0,
                       cell1,
                       cell2,
                       cell3,
                       cell4,
                       cell5,
                       cell6,
                       cell7);
}

void CellOAB::setValues_am2(dsBatteryCell *cell0, dsBatteryCell *cell1, dsBatteryCell *cell2, dsBatteryCell *cell3, dsBatteryCell *cell4, dsBatteryCell *cell5, dsBatteryCell *cell6, dsBatteryCell *cell7)
{
    ui->Am2->setValues(cell0,
                       cell1,
                       cell2,
                       cell3,
                       cell4,
                       cell5,
                       cell6,
                       cell7);
}

void CellOAB::setValues_am3(dsBatteryCell *cell0, dsBatteryCell *cell1, dsBatteryCell *cell2, dsBatteryCell *cell3, dsBatteryCell *cell4, dsBatteryCell *cell5, dsBatteryCell *cell6, dsBatteryCell *cell7)
{
    ui->Am3->setValues(cell0,
                       cell1,
                       cell2,
                       cell3,
                       cell4,
                       cell5,
                       cell6,
                       cell7);
}
