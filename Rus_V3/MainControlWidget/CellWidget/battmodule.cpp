#include "battmodule.h"
#include "ui_battmodule.h"

BattModule::BattModule(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BattModule)
{
    ui->setupUi(this);

    ui->Cell_0->setInfoCellNum(1);
    ui->Cell_1->setInfoCellNum(2);
    ui->Cell_2->setInfoCellNum(3);
    ui->Cell_3->setInfoCellNum(4);
    ui->Cell_4->setInfoCellNum(5);
    ui->Cell_5->setInfoCellNum(6);
    ui->Cell_6->setInfoCellNum(7);
    ui->Cell_7->setInfoCellNum(8);
}

BattModule::~BattModule()
{
    delete ui;
}

void BattModule::setValues(dsBatteryCell *cell0, dsBatteryCell *cell1, dsBatteryCell *cell2, dsBatteryCell *cell3, dsBatteryCell *cell4, dsBatteryCell *cell5, dsBatteryCell *cell6, dsBatteryCell *cell7)
{
    ui->Cell_0->updateValue(cell0);
    ui->Cell_1->updateValue(cell1);
    ui->Cell_2->updateValue(cell2);
    ui->Cell_3->updateValue(cell3);
    ui->Cell_4->updateValue(cell4);
    ui->Cell_5->updateValue(cell5);
    ui->Cell_6->updateValue(cell6);
    ui->Cell_7->updateValue(cell7);


    if(((cell0->infoReg&32768) == 0) ||
            ((cell1->infoReg&32768) == 0) ||
            ((cell2->infoReg&32768) == 0) ||
            ((cell3->infoReg&32768) == 0) ||
            ((cell4->infoReg&32768) == 0) ||
            ((cell5->infoReg&32768) == 0) ||
            ((cell6->infoReg&32768) == 0) ||
            ((cell7->infoReg&32768) == 0) )
    {
        ui->box->setFlat(true);
        ui->dumpWidget->setDisabled(true);

    }else
    {
        ui->box->setFlat(false);
        ui->dumpWidget->setEnabled(true);

        if(((cell0->infoReg&32) == 0) ||
                ((cell1->infoReg&32) == 0) ||
                ((cell2->infoReg&32) == 0) ||
                ((cell3->infoReg&32) == 0) ||
                ((cell4->infoReg&32) == 0) ||
                ((cell5->infoReg&32) == 0) ||
                ((cell6->infoReg&32) == 0) ||
                ((cell7->infoReg&32) == 0) )
        {
            ui->dumpWidget->setStyleSheet("background-color:rgb(85, 255, 0);");

        }
        else
        {
            ui->dumpWidget->setStyleSheet("background-color:red;");
        }


        ui->lblNameModule->setText("Аккумуляторный модуль №" + QString::number(cell0->section,'i',0));
    }



}
