#include "infocell.h"
#include "ui_infocell.h"
#include "RusNamespace.h"

InfoCell::InfoCell(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InfoCell)
{
    ui->setupUi(this);
    step = 0;
    buff[0] = 0;
    buff[1] = 1;
    ui->balance->setVisible(false);
}

InfoCell::~InfoCell()
{
    delete ui;
}

void InfoCell::updateValue(dsBatteryCell *cell)
{

    if(!checkValid(cell))
        return;
    updateVisionCell(cell);
    updateBalanceText(cell);
    updtaeValuseLbl(cell);
}

bool InfoCell::checkValid(dsBatteryCell *cell)
{

    if(cell->infoReg&32768)
    {
        ui->groupBox->setFlat(false);
        ui->groupBox->setEnabled(true);
        return true;

    }else
    {
        ui->groupBox->setFlat(true);
        ui->groupBox->setDisabled(true);
        return false;
    }
}

void InfoCell::updateVisionCell(dsBatteryCell *cell)
{

    value = step&1;

    int flagVlt = checkVltFlag(cell->voltage);
    int flagTemp0 = checkTempFlag(cell->temp_0);
    int flagTemp1 = checkTempFlag(cell->temp_1);
    int allFlag = flagVlt | flagTemp0 | flagTemp1;
    buff[value] = allFlagResult(allFlag);
    cell->flagErrors = buff[value];

    if(buff[0] != buff[1])
    {


        if(buff[value] ==  RS::Normal){
            ui->groupBox->setStyleSheet("background-color: white;"
                                         "color:black;");
        }
        if(buff[value] ==  RS::Warning){
            ui->groupBox->setStyleSheet("background-color: yellow;"
                                         "color:black;");
        }
        if(buff[value] ==  RS::Emergency){
            ui->groupBox->setStyleSheet("background-color: rgb(255, 0, 0);"
                                        "color:white;");
        }
    }
    step ++;
}

void InfoCell::updateBalanceText(dsBatteryCell *cell)
{
    if(cell->infoReg&16)
    {
        ui->balance->setVisible(true);
    }
    else
    {
        ui->balance->setVisible(false);
    }
}

void InfoCell::updtaeValuseLbl(dsBatteryCell *cell)
{
    ui->voltage->setText(QString::number((float)cell->voltage/1000,'f',2) + " В");
    ui->temp0->setText(QString::number(cell->temp_0,'i',0) + " °C");
    ui->temp1->setText(QString::number(cell->temp_1,'i',0) + " °C");
}

int InfoCell::checkTempFlag(int value)
{
    if(value > 65 || value < -4){
        return RS::Emergency;
    }
    else if(value > 61)
    {
        return RS::Warning;
    }
    return RS::Normal;
}

int InfoCell::checkVltFlag(int value)
{
    if(value > 4210 || value < 2810){
        return RS::Emergency;
    }
    else if((value < 3100 && value >= 2810)||
        (value > 4200 && value <= 4210))
    {
        return RS::Warning;
    }
    return RS::Normal;
}

int InfoCell::allFlagResult(int aflag)
{
    if(aflag&RS::Emergency)
    {
        return RS::Emergency;
    }
    else if (aflag&RS::Warning)
    {
        return RS::Warning;
    }
    else
    {
        return RS::Normal;
    }

}

void InfoCell::setInfoCellNum(int num)
{
    ui->numCell->setText(QString::number(num,'i',0));
}
