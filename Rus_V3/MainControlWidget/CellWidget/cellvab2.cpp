#include "cellvab2.h"
#include "ui_cellvab2.h"

CellVAB2::CellVAB2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CellVAB2)
{
    ui->setupUi(this);
}

CellVAB2::~CellVAB2()
{
    delete ui;
}

void CellVAB2::setValues(dsBatteryCell *cell0, dsBatteryCell *cell1, dsBatteryCell *cell2, dsBatteryCell *cell3, dsBatteryCell *cell4, dsBatteryCell *cell5, dsBatteryCell *cell6, dsBatteryCell *cell7)
{
    ui->Am0->setValues(cell0,
                       cell1,
                       cell2,
                       cell3,
                       cell4,
                       cell5,
                       cell6,
                       cell7);
}
