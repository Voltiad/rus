#ifndef BATTMODULE_H
#define BATTMODULE_H

#include <QWidget>
#include <datastore.h>

namespace Ui {
class BattModule;
}

class BattModule : public QWidget
{
    Q_OBJECT

public:
    explicit BattModule(QWidget *parent = 0);
    ~BattModule();

private:
    Ui::BattModule *ui;

public:
    void setValues(dsBatteryCell*cell0,
                   dsBatteryCell*cell1,
                   dsBatteryCell*cell2,
                   dsBatteryCell*cell3,
                   dsBatteryCell*cell4,
                   dsBatteryCell*cell5,
                   dsBatteryCell*cell6,
                   dsBatteryCell*cell7);
};

#endif // BATTMODULE_H
