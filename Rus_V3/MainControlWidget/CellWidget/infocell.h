#ifndef INFOCELL_H
#define INFOCELL_H

#include <QWidget>
#include <datastore.h>

namespace Ui {
class InfoCell;
}

class InfoCell : public QWidget
{
    Q_OBJECT

public:
    explicit InfoCell(QWidget *parent = 0);
    ~InfoCell();

private:
    Ui::InfoCell *ui;

public:
    //void updateValue(dsBattery *);
    void updateValue(dsBatteryCell *);

private:
    bool checkValid(dsBatteryCell *);
    void updateVisionCell(dsBatteryCell *);
    void updateBalanceText(dsBatteryCell *);
    void updtaeValuseLbl(dsBatteryCell *);

    int checkTempFlag(int value);
    int checkVltFlag(int value);
    int allFlagResult(int aflag);

    int step;
    int buff[2];
    int value;

public:
    void setInfoCellNum(int num);
};

#endif // INFOCELL_H
