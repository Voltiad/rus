#ifndef CELLOAB_H
#define CELLOAB_H

#include <QWidget>
#include "datastore.h"

namespace Ui {
class CellOAB;
}

class CellOAB : public QWidget
{
    Q_OBJECT

public:
    explicit CellOAB(QWidget *parent = 0);
    ~CellOAB();

private:
    Ui::CellOAB *ui;

public:
    void setValues_am0(dsBatteryCell*cell0,
                   dsBatteryCell*cell1,
                   dsBatteryCell*cell2,
                   dsBatteryCell*cell3,
                   dsBatteryCell*cell4,
                   dsBatteryCell*cell5,
                   dsBatteryCell*cell6,
                   dsBatteryCell*cell7);

    void setValues_am1(dsBatteryCell*cell0,
                   dsBatteryCell*cell1,
                   dsBatteryCell*cell2,
                   dsBatteryCell*cell3,
                   dsBatteryCell*cell4,
                   dsBatteryCell*cell5,
                   dsBatteryCell*cell6,
                   dsBatteryCell*cell7);

    void setValues_am2(dsBatteryCell*cell0,
                   dsBatteryCell*cell1,
                   dsBatteryCell*cell2,
                   dsBatteryCell*cell3,
                   dsBatteryCell*cell4,
                   dsBatteryCell*cell5,
                   dsBatteryCell*cell6,
                   dsBatteryCell*cell7);

    void setValues_am3(dsBatteryCell*cell0,
                   dsBatteryCell*cell1,
                   dsBatteryCell*cell2,
                   dsBatteryCell*cell3,
                   dsBatteryCell*cell4,
                   dsBatteryCell*cell5,
                   dsBatteryCell*cell6,
                   dsBatteryCell*cell7);
};

#endif // CELLOAB_H
