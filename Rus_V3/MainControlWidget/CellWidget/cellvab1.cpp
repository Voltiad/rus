#include "cellvab1.h"
#include "ui_cellvab1.h"

CellVAB1::CellVAB1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CellVAB1)
{
    ui->setupUi(this);
}

CellVAB1::~CellVAB1()
{
    delete ui;
}

void CellVAB1::setValues(dsBatteryCell *cell0, dsBatteryCell *cell1, dsBatteryCell *cell2, dsBatteryCell *cell3, dsBatteryCell *cell4, dsBatteryCell *cell5, dsBatteryCell *cell6, dsBatteryCell *cell7)
{
    ui->Am0->setValues(cell0,
                       cell1,
                       cell2,
                       cell3,
                       cell4,
                       cell5,
                       cell6,
                       cell7);
}
