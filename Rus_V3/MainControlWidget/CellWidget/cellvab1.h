#ifndef CELLVAB1_H
#define CELLVAB1_H

#include <QWidget>
#include "datastore.h"

namespace Ui {
class CellVAB1;
}

class CellVAB1 : public QWidget
{
    Q_OBJECT

public:
    explicit CellVAB1(QWidget *parent = 0);
    ~CellVAB1();

private:
    Ui::CellVAB1 *ui;

public:
    void setValues(dsBatteryCell*cell0,
                   dsBatteryCell*cell1,
                   dsBatteryCell*cell2,
                   dsBatteryCell*cell3,
                   dsBatteryCell*cell4,
                   dsBatteryCell*cell5,
                   dsBatteryCell*cell6,
                   dsBatteryCell*cell7);
};

#endif // CELLVAB1_H
