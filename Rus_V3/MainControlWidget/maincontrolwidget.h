#ifndef MAINCONTROLWIDGET_H
#define MAINCONTROLWIDGET_H

#include <QWidget>
#include <QKeyEvent>
#include <QButtonGroup>
#include <QDate>
#include <QTime>
#include <QMutex>

#include "datastore.h"

namespace Ui {
class MainControlWidget;
}

class MainControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainControlWidget(QWidget *parent = 0);
    ~MainControlWidget();

private:
    void windowSetting();
    void generalSetting();

public:
    void lateSetting();

    void updateValuesBatts();

public:
    dsChargeModule *dsOAB_disp;
    dsChargeModule *dsVAB1_disp;
    dsChargeModule *dsVAB2_disp;

    dsModuleControle *dsMcOAB_disp;
    dsModuleControle *dsMcVAB1_disp;
    dsModuleControle *dsMcVAB2_disp;
    dsBattery *dsMcBatt_disp;

    dsBattManagSystem *dsBMS_disp;
    dsBattery *dsBmsBattery_disp;

    void resetButtons();

private:
    void timerEvent(QTimerEvent *event);
private:
    void keyPressEvent(QKeyEvent *);

private:
    int timerDateTime;
    int updateDisplay;

public:
    void settingDateTime();
    void startDisplayTimer();
    void stopDisplayTimer();

    int numMode;
    void setNumMode(int value){numMode = value;}
    int getNumMode(){return numMode;}

    void setCentralWidget(int numMode);

private:
    void settingButtons();

private slots:
    void buttonClickDisplay(int index);
    void clickOnButtonReturn();

public:

    void setHeaderText(QString text);

signals:
    void goToWorkMode();

public slots:
    void updateWorkTimeLbl(int currentWorkTime);

private:
    Ui::MainControlWidget *ui;
    QButtonGroup *mainButtonGroup;

private:
     void settingSignalsPowerButtons();

private:
     bool powerBtnOAB;
     bool powerBtnVAB1;
     bool powerBtnVAB2;

signals:
    void powerOnOAB();
    void powerOffOAB();

    void powerOnVAB1();
    void powerOffVAB1();

    void powerOnVAB2();
    void powerOffVAB2();

    void startBalanceOAB();
    void stopBalanceOAB();

    void startBalanceVAB1();
    void stopBalanceVAB1();

    void startBalanceVAB2();
    void stopBalanceVAB2();


    void updateVoltageOAB(int value);
    void updateVoltageVAB1(int value);
    void updateVoltageVAB2(int value);

signals:
    void closeProg();

private:
    QTime *time;
    int timeElapsed;

};

#endif // MAINCONTROLWIDGET_H
