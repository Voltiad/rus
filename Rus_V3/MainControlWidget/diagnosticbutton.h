#ifndef DIAGNOSTICBUTTON_H
#define DIAGNOSTICBUTTON_H

#include <QPushButton>
#include <diagnosticwindow.h>
#include <datastore.h>

class DiagnosticButton : public QPushButton
{
    Q_OBJECT
public:
    explicit DiagnosticButton(QWidget *parent = 0);

signals:

public slots:
    void clickOnbutton();

private:
    DiagnosticWindow *diagWin;

public:
    dsChargeModule *dsCharger;
    dsModuleControle *dsMoudCont;
    dsBattManagSystem *dsBMS;
    dsBattery *dsBmsBattery;

    dsFlagsDiagnostic *flags;

    void updateMemoryDiagnosticWidget();

public:
    void checkErrors(int num);

private:
    int numMode;
    void setNumMode(int num) {numMode = num;}
    int getNumMode(){return numMode;}

public:
    void setRangeBatts(int min,int max){minRange = min; maxRange = max;}
    int getMinRange(){return minRange;}
    int getMaxRange(){return maxRange;}

private:
    int minRange;
    int maxRange;

private:
    void setCustomStyleButton(QString css_style,QString textButton);

};

#endif // DIAGNOSTICBUTTON_H
