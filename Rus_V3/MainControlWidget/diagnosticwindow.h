#ifndef DIAGNOSTICWINDOW_H
#define DIAGNOSTICWINDOW_H

#include <QWidget>
#include <datastore.h>
#include <QLabel>
#include <RusNamespace.h>

namespace Ui {
class DiagnosticWindow;
}

template <typename deviceName>
bool connectionStatusDevice(deviceName *device,QLabel *label)
{
    if(device->connectionStatus){
        label->setText(connectionYes);
        label->setStyleSheet(textColorGreen);
        return true;
    }else{
        label->setText(connectionNo);
        label->setStyleSheet(textColorRed);
        return false;
    }
}

class DiagnosticWindow : public QWidget
{
    Q_OBJECT

public:
    explicit DiagnosticWindow(QWidget *parent = 0);
    ~DiagnosticWindow();

private slots:
    void on_close_clicked();

public:
    dsChargeModule *dsCharger;
    dsModuleControle *dsMoudCont;
    dsBattManagSystem *dsBMS;
    dsBattery *dsBmsBattery;

    dsFlagsDiagnostic *flags;


    void updateInfo();
    void setNumMode(int num){numMode = num;}
    int getNumMode(){return numMode;}
    int numMode;

private:
    void updateVisibleButtons();

    void visibleLabel(QLabel *lblName, QLabel *lblValue, bool value);
    void visibleBMS(bool value);
    void visibleCharge(bool value);
    void visibleMC(bool value);

private:
    Ui::DiagnosticWindow *ui;

public:
    void setRangeBatts(int min,int max){minRange = min; maxRange = max;}
    int getMinRange(){return minRange;}
    int getMaxRange(){return maxRange;}

private:
    int minRange;
    int maxRange;

private:
    QString infoText;

    void testValidBatt();
    void testStatusBatt();
    void testConnectionCableCharge();

public:
    int statusSystem();

};

#endif // DIAGNOSTICWINDOW_H
