#include "controlmodulewidget.h"
#include "ui_controlmodulewidget.h"
#include "capacitive_characteristic.h"
#include <QGraphicsDropShadowEffect>
#include "dialog.h"
#include "dialogonebtn.h"
#include <QDebug>
#include <RusNamespace.h>

ControlModuleWidget::ControlModuleWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlModuleWidget)
{
    ui->setupUi(this);
    time = new QTime();
    InitTableVoltageCapacity();

    setDisable();
    elapseTime = 0;
    time->start();

    QGraphicsDropShadowEffect *ShadowBtn;
    ShadowBtn = new QGraphicsDropShadowEffect();
    ShadowBtn->setBlurRadius(BlurRadiusShadow);
    ShadowBtn->setColor(ColorShadow);
    ShadowBtn->setOffset(OffsetShadow);
    ui->powerButton->setGraphicsEffect(ShadowBtn);

    flags = new dsFlagsDiagnostic();

    ui->infoButton->flags = flags;
}


ControlModuleWidget::~ControlModuleWidget()
{
    delete ui;
}

void ControlModuleWidget::updateInfoButtonMemory()
{
    ui->infoButton->dsBMS = dsBMS;
    ui->infoButton->dsBmsBattery = dsBmsBattery;
    ui->infoButton->dsCharger = dsCharger;
    ui->infoButton->dsMoudCont = dsMoudCont;

    ui->infoButton->updateMemoryDiagnosticWidget();
}

void ControlModuleWidget::setDisable()
{
    ui->groupBox->setFlat(true);
    ui->powerButton->setDisabled(true);
}

void ControlModuleWidget::setEnabled()
{

    ui->groupBox->setFlat(false);
    ui->powerButton->setDisabled(false);
}

void ControlModuleWidget::resetButtons()
{
    ui->powerButton->setChecked(false);
}

void ControlModuleWidget::setCentralWidget(int num)
{
    if(num == RS::LiOnChargeBalanceAll      ||
       num == RS::LiOnChargeBalanceModule   ||
       num == RS::LiOnChargeNoBalanceAll    ||
       num ==RS::LiOnChargeNoBalanceModule  ||
       num ==RS::LiOnDischarge)
    {
        ui->centralWidget->setCurrentIndex(0);
        setHeaderCentralWidget("Заряд батареи");
    }
    if(num ==RS::PbACChargeBalanceAll       ||
       num ==RS::PbACChargeBalanceModule    ||
       num ==RS::PbACChargeNoBalanceAll     ||
       num ==RS::PbACChargeNoBalanceModule  ||
       num ==RS::PbACDischarge)
    {
        ui->centralWidget->setCurrentIndex(1);
        setHeaderCentralWidget("Состояние батареи");

    }
    if(RS::VoltageSource == num)
    {
        ui->centralWidget->setCurrentIndex(2);
        setHeaderCentralWidget("Уставки тока/напряжения");
    }

    numMode = num;
    setTextButton(num);
}

void ControlModuleWidget::setTextButton(int num)
{
    if(num == RS::LiOnChargeBalanceAll ||
            num == RS::LiOnChargeBalanceModule ||
            num == RS::LiOnChargeNoBalanceAll ||
            num == RS::LiOnChargeNoBalanceModule ||
            num == RS::PbACChargeBalanceAll ||
            num == RS::PbACChargeBalanceModule ||
            num == RS::PbACChargeNoBalanceAll ||
            num == RS::PbACChargeNoBalanceModule)
    {
        ui->AddString->setText(addStringCharge);

        if(ui->powerButton->isChecked())
        {
            ui->powerButton->setText(stopAction + startCharge.toLower());

        }else
        {
            ui->powerButton->setText(startCharge);
        }


    }

    if(num == RS::LiOnDischarge || num == RS::PbACDischarge)
    {
        if(ui->powerButton->isChecked())
        {
            ui->powerButton->setText(stopAction + startDischarge.toLower());
        }else{
            ui->powerButton->setText(startDischarge);
        }

        ui->AddString->setText(addStringDischarge);
    }
}

void ControlModuleWidget::setHeaderModule(QString text)
{
    ui->headerModule->setText("Текущее состояние " + text);
    nameModule = text;

    if(nameModule == Text_OAB){
        ui->infoButton->setRangeBatts(0,32);}
    if(nameModule == Text_VAB1){
        ui->infoButton->setRangeBatts(40,48);}
    if(nameModule == Text_VAB2) {
        ui->infoButton->setRangeBatts(32,40);}
}

void ControlModuleWidget::setHeaderCentralWidget(QString text)
{
    ui->headerCentralWidget->setText(text);
}

void ControlModuleWidget::on_powerButton_clicked(bool checked)
{
    emit powerButton(checked);
    if(checked == true){
        ampersLoad = 0;
        workTime = 0;
    }
}

void ControlModuleWidget::setDisplayValue(int voltage, int current)
{
    float vlt = (float)voltage/100;
    float curr = (float)current/100;
    ui->valueVoltage->setText(QString::number(vlt,'f',1));
    ui->valueCurrent->setText(QString::number(curr,'f',1));
}

void ControlModuleWidget::setCentralWidgetValuePerCent(int currCap, int maxCap)
{
    ui->percent->setValues(currCap,maxCap);
}

void ControlModuleWidget::on_powerButton_clicked()
{
    //if(dialogSetting()==0) return;
    settingTextButtons();
}


void ControlModuleWidget::updateAlg()
{

    switch (numMode)
    {
        case RS::LiOnChargeBalanceAll:
            algLiOnChargeBattWithBalance();
            break;

        case RS::LiOnChargeNoBalanceAll:
            algLiOnChargeBattWithoutBalance();
            break;

        case RS::LiOnChargeBalanceModule:
            algLiOnChargeModuleWhitBalance();
            break;

        case RS::LiOnChargeNoBalanceModule:
            algLiOnChargeModuleWhitoutBalance();
            break;

        case RS::LiOnDischarge:
            algLiOnDischrage();
            break;

        default:

            break;
    }

    ui->infoButton->checkErrors(numMode);

    elapseTime = time->elapsed();
    time->restart();
}

void ControlModuleWidget::algLiOnChargeModuleWhitBalance()
{
    if(nameModule == Text_OAB){
        setDisable();
        return;
    }

    if(checkConectionChargeModule()){checkFlags();}
    findLowAndHighBatt();
    updateCentralValue();
    updateValues();

    chargeOver=false;
    if(dsMoudCont->chargeMode == 0)
    {
        emit startBalance();
    }

    if(ui->powerButton->isChecked())
    {

        updateLabelWorkTime();
        checkMaxBattAndCorrectionVoltage();
        chargeDownCurrent();
    }
    else
    {
        finalStageFlag = false;
    }

    if (chargeOver) {
        stopBalance();
    }
}

void ControlModuleWidget::algLiOnChargeModuleWhitoutBalance()
{

    if(nameModule == Text_OAB){
        setDisable();
        return;
    }

    if(checkConectionChargeModule()){checkFlags();}
    findLowAndHighBatt();
    updateCentralValue();
    updateValues();

    emit stopBalance();

    if(ui->powerButton->isChecked())
    {
        updateLabelWorkTime();
        checkMaxBattAndCorrectionVoltage();
        chargeDownCurrent();
    }
    else
    {
        finalStageFlag = false;
    }


}



void ControlModuleWidget::algLiOnChargeBattWithoutBalance()
{
    if(checkConectionChargeBMS()){checkFlags();}
    findLowAndHighBatt();
    updateCentralValue();
    updateValues();

    emit stopBalance();

    if(ui->powerButton->isChecked())
    {
        updateLabelWorkTime();
        checkMaxBattAndCorrectionVoltage();
        chargeDownCurrent();
    }
    else
    {
        finalStageFlag = false;
    }
}

void ControlModuleWidget::algLiOnChargeBattWithBalance()
{
    if(checkConectionChargeBMS()){checkFlags();}
    findLowAndHighBatt();
    updateCentralValue();
    updateValues();

    chargeOver=false;
    if(dsBMS->chargeMode == 0)
    {
        emit startBalance();
    }

    if(ui->powerButton->isChecked())
    {
        updateLabelWorkTime();
        checkMaxBattAndCorrectionVoltage();
        chargeDownCurrent();
    }
    else
    {
        finalStageFlag = false;
    }

    if (chargeOver) {
        stopBalance();
    }
}

void ControlModuleWidget::algLiOnDischrage()
{
    if(checkInfoDevice(dsMoudCont))
    {
        checkValid();
        if(validBatt){
            setEnabled();
        }else
        {
            setDisable();
        }
    } else
    {
        setDisable();
    }

    findLowAndHighBatt();
    updateCentralValue();
    updateValues();

    if(ui->powerButton->isChecked())
    {
        if(lowBattVlt < 3100)
        {
            ui->powerButton->click();
            on_powerButton_clicked(false);
            showOneBtnDialog(ampersLoad,workTime);

        }else
        {
            finalStageFlag = false;
        }

        if(finalStageFlag){

        }
        else
        {
            if(currentCurrent(dsMoudCont->currentOne,0) < 0)
            {
                if(elapseTime != 0)
                {
                    ampersLoad += ui->valueCurrent->text().toFloat()/3600000*(float)elapseTime;
                    if(ampersLoad>10){
                        ui->valueAddString->setText(QString::number(ampersLoad,'f',1));
                    }else{
                        ui->valueAddString->setText(QString::number(ampersLoad,'f',2));
                    }
                }
               updateLabelWorkTime();
            }
        }
    }else{
        finalStageFlag = false;
    }

    if(dsMoudCont->dbus == 2)
    {
        if(ui->powerButton->isChecked()){

        }else{
            on_powerButton_clicked(false);
        }
    }
    if(dsMoudCont->dbus == 0)
    {
        if(ui->powerButton->isChecked())
        {
            on_powerButton_clicked(true);

        }
    }
}

void ControlModuleWidget::checkMaxBattAndCorrectionVoltage()
{
    if(highBattVlt < 4200){
        if(finalStageFlag == false){ emit updateVoltage(sumBatt + 380); }
    }
    else{ finalStageFlag = true; }

}

void ControlModuleWidget::chargeDownCurrent()
{
    if(finalStageFlag)
    {
        int maxCurrent = 10;
        if(currentCurrent(dsCharger->currentOne,
                          dsCharger->currentTwo)>maxCurrent)
        {

            if(highBattVlt>4210 ){  correctionVoltage(50);}
            else if (highBattVlt<4210 && highBattVlt > 4200) { correctionVoltage(5);}
            else{
            }
        }
        else
        {   chargeOver = true;
            ui->powerButton->click();
            on_powerButton_clicked(false);
            showOneBtnDialog(ampersLoad,workTime);
        }
    }
}

void ControlModuleWidget::correctionVoltage(int value)
{
    int uRef = (dsCharger->u_voltage)-value;
    emit updateVoltage(uRef);
}

bool ControlModuleWidget::checkConectionChargeModule()
{
    ui->infoButton->setEnabled(true);

    if(checkInfoDevice(dsMoudCont)){
        checkValid();

        if(checkConnectionCharger()){
            if(validBatt){
                if(dsCharger->voltageOne > 1){
                    setEnabled();
                    return true;
                }
                setDisable();
                return false;
            }
            else{
                setDisable();
                return false;
            }
        }
        else    return false;
    }
    else    return false;

}

bool ControlModuleWidget::checkConectionChargeBMS()
{
    ui->infoButton->setEnabled(true);

    if(checkInfoDevice(dsBMS)){
        checkValid();

        if(checkConnectionCharger()){
            if(validBatt){
                if(dsCharger->voltageOne > 1){
                    setEnabled();
                    return true;
                }
                setDisable();
                return false;
            }
            else{
                setDisable();
                return false;
            }
        }
        else    return false;
    }
    else    return false;
}

bool ControlModuleWidget::checkInfoDevice(dsModuleControle *device)
{
    if(device->connectionStatus) return true;
    else  return false;
}

bool ControlModuleWidget::checkInfoDevice(dsBattManagSystem *device)
{
    if(device->connectionStatus){
       return true;
    } else{
        setDisable();
        return false;
    }
}

bool ControlModuleWidget::checkConnectionCharger()
{
    if(dsCharger->connectionStatus == true)
    {
        if(nameModule == Text_OAB)
        {

            if(ui->powerButton->isChecked())
            {
                if((dsCharger->workeModeOne == 0 && dsCharger->workeModeTwo == 0) ||
                        (dsCharger->workeModeOne == 0) ||
                        (dsCharger->workeModeTwo == 0) ){
                    on_powerButton_clicked(true);

                }
            }else{
                if((dsCharger->workeModeOne == 3 && dsCharger->workeModeTwo == 3) ||
                        (dsCharger->workeModeOne == 3) ||
                        (dsCharger->workeModeTwo == 3) ){
                    on_powerButton_clicked(true);

                }
            }
        }else{
            if(dsCharger->workeModeOne == 3)
            {
                if(ui->powerButton->isChecked()){

                }else{
                    on_powerButton_clicked(false);
                    qDebug() << "try to stop";
                }
            }
            if(dsCharger->workeModeOne == 0)
            {
                if(ui->powerButton->isChecked())
                {
                    on_powerButton_clicked(true);
                    qDebug() << "try to start";

                }
            }
        }
        return true;
    }else{
        return false;
    }
}

void ControlModuleWidget::checkValid()
{
    if(nameModule == Text_OAB) {
        validBatt = checkValidBatt(0,32); }
    if(nameModule == Text_VAB1){
        validBatt = checkValidBatt(40,48); }
    if(nameModule == Text_VAB2){
        validBatt = checkValidBatt(32,40); }
}

bool ControlModuleWidget::checkValidBatt(int minAdr, int maxAdr)
{
    for (int var = minAdr; var < maxAdr; ++var)
    {
       if((dsBmsBattery->cell[var].infoReg&-32768) == 0){
           flags->validCells = false;
           return false;
       }
    }
    flags->validCells = true;
    return true;
}

void ControlModuleWidget::checkFlags()
{
    if(nameModule == Text_OAB){
        workStatus = checkFlagsBatt(0,32);}
    if(nameModule == Text_VAB1){
        workStatus = checkFlagsBatt(40,48);}
    if(nameModule == Text_VAB2) {
        workStatus = checkFlagsBatt(32,40);}
}

bool ControlModuleWidget::checkFlagsBatt(int minAdr,int maxAdr)
{
    for (int var = minAdr; var < maxAdr; ++var) {
        if(dsBmsBattery->cell[var].infoReg&1 ||
                dsBmsBattery->cell[var].infoReg&2 ||
                dsBmsBattery->cell[var].infoReg&4 ||
                dsBmsBattery->cell[var].infoReg&8 ||
                dsBmsBattery->cell[var].infoReg&32)
        {
         if(dsBmsBattery->cell[var].infoReg&1){flags->highTemp = true;}
         if(dsBmsBattery->cell[var].infoReg&2){flags->lowTemp = true;}
         if(dsBmsBattery->cell[var].infoReg&4){flags->highVoltage = true;}
         if(dsBmsBattery->cell[var].infoReg&8){flags->lowVoltage = true;}
         if(dsBmsBattery->cell[var].infoReg&32){flags->dumpSensor = true;}
         return false;
        }else{
            if(dsBmsBattery->cell[var].infoReg&1){flags->highTemp = false;}
            if(dsBmsBattery->cell[var].infoReg&2){flags->lowTemp = false;}
            if(dsBmsBattery->cell[var].infoReg&4){flags->highVoltage = false;}
            if(dsBmsBattery->cell[var].infoReg&8){flags->lowVoltage = false;}
            if(dsBmsBattery->cell[var].infoReg&32){flags->dumpSensor = false;}
        }
    }

    return true;
}

void ControlModuleWidget::findLowAndHighBatt()
{
    if(validBatt)
    {
        if(nameModule == Text_OAB)
        {
            lowBattVlt = finfLowVlt(0,32);
            highBattVlt = finfHighVlt(0,32);
            sumBatt = findSummBatt(0,32);

        }
        if(nameModule == Text_VAB1)
        {
            lowBattVlt = finfLowVlt(40,48);
            highBattVlt = finfHighVlt(40,48);
            sumBatt = findSummBatt(40,48);
        }
        if(nameModule == Text_VAB2)
        {
            lowBattVlt = finfLowVlt(32,40);
            highBattVlt = finfHighVlt(32,40);
            sumBatt = findSummBatt(32,40);
        }
    }
}

int ControlModuleWidget::finfLowVlt(int minAdr, int maxAdr)
{
    int value;
    for (int var = minAdr; var < maxAdr; ++var)
    {
        if(var == minAdr) {
            value = dsBmsBattery->cell[var].voltage;
        }else{
            if(value > dsBmsBattery->cell[var].voltage){
                value = dsBmsBattery->cell[var].voltage;
            }
        }
    }
    return value;
}

int ControlModuleWidget::finfHighVlt(int minAdr, int maxAdr)
{
    int value;
    for (int var = minAdr; var < maxAdr; ++var)
    {
        if(var == minAdr) {
            value = dsBmsBattery->cell[var].voltage;
        }else{
            if(value < dsBmsBattery->cell[var].voltage){
                value = dsBmsBattery->cell[var].voltage;
            }
        }
    }
    return value;
}

int ControlModuleWidget::findSummBatt(int minAdr, int maxAdr)
{
    int value;
    for (int var = minAdr; var < maxAdr; ++var){
        if(var == minAdr) {
            value = dsBmsBattery->cell[var].voltage;
        }else{
            value += dsBmsBattery->cell[var].voltage;
        }
    }
    value = value / 10;
    return value;
}

void ControlModuleWidget::updateValues()
{
    if(numMode == RS::LiOnDischarge || numMode == RS::PbACDischarge)
    {
        float voltage = (float)sumBatt/100;
        float current = (float)dsMoudCont->currentOne/100 * -1;
        float actualCurrent;

        if(dsMoudCont->dbus == 2){
            if(current > 0 && current <= 2.5){
                actualCurrent = 2.5;
            }
            if(current > 2.5 && current <= 5){
                actualCurrent = 5;
            }
            if(current > 5 && current <= 7.5){
                actualCurrent = 7.5;
            }
            if(current > 7.5 && current <= 10){
                actualCurrent = 10;
            }
            if(current > 10 && current <= 12.5){
                actualCurrent = 12.5;
            }
            if(current > 12.5 && current <= 15){
                actualCurrent = 15;
            }
            if(current > 15 && current <= 17.5){
                actualCurrent = 17.5;
            }
            if(current > 17.5 && current <= 20){
                actualCurrent = 20;
            }
            if(current > 20 && current <= 22.5){
                actualCurrent = 22.5;
            }
        }


        ui->valueVoltage->setText(QString::number(voltage,'f',1));
        ui->valueCurrent->setText(QString::number(actualCurrent,'f',1));
    }
    else
    {
        float voltage = (float)dsCharger->voltageOne/100;
        float allCurrent =  (float)dsCharger->currentOne/100 + (float)dsCharger->currentTwo/100;
        ui->valueVoltage->setText(QString::number(voltage,'f',1));
        ui->valueCurrent->setText(QString::number(allCurrent,'f',1));
    }
}


void ControlModuleWidget::updateCentralValue()
{
    switch (numMode)
    {
        case RS::LiOnChargeBalanceAll:
            updtaePercentWidget(lowBattVlt);
            break;

        case RS::LiOnChargeNoBalanceAll:
            updtaePercentWidget(lowBattVlt);
            break;

        case RS::LiOnChargeBalanceModule:
            updtaePercentWidget(lowBattVlt);
            break;

        case RS::LiOnChargeNoBalanceModule:
            updtaePercentWidget(lowBattVlt);
            break;

        case RS::LiOnDischarge:
            updtaePercentWidget(lowBattVlt);
            break;

        default:

            break;
    }
}

void ControlModuleWidget::updtaePercentWidget(int value)
{
    float maxCap =  470;
    float allCurrent =  currentCurrent(dsCharger->currentOne,
                                       dsCharger->currentTwo);
    if(allCurrent > 5)
    {
        if(elapseTime != 0)
        {
            ampersLoad += allCurrent/3600000*(float)elapseTime;
            if(ampersLoad>10){
                ui->valueAddString->setText(QString::number(ampersLoad,'f',1));
            }else{
                ui->valueAddString->setText(QString::number(ampersLoad,'f',2));
            }

        }
    }

      float capPerCent = VoltageToCapacity(value);
      currCap =  maxCap*capPerCent/100;


      ui->percent->setValues(currCap,maxCap);
}

void ControlModuleWidget::updateLabelWorkTime()
{
    workTime ++;
    unsigned int min = workTime/60%60;
    unsigned int hours = workTime/60/60;

    if(workTime<60){
        ui->valueTimeStart->setText( QString::number(workTime) + " сек.");
    }else{
        ui->valueTimeStart->setText(QString::number(hours) + " ч. " +
                              QString::number(min) + " м.");
    }
}


void ControlModuleWidget::on_infoButton_clicked()
{
}

bool ControlModuleWidget::dialogSetting()
{
    Dialog *dialog = new Dialog(" ");

    if(numMode == RS::LiOnChargeBalanceAll ||
            numMode == RS::LiOnChargeBalanceModule ||
            numMode == RS::LiOnChargeNoBalanceAll ||
            numMode == RS::LiOnChargeNoBalanceModule ||
            numMode == RS::PbACChargeBalanceAll ||
            numMode == RS::PbACChargeBalanceModule ||
            numMode == RS::PbACChargeNoBalanceAll ||
            numMode == RS::PbACChargeNoBalanceModule)
    {
        if(ui->powerButton->isChecked())
        {
            dialog->setText(stopAction + startCharge.toLower() + nameModule );
        }else{
            dialog->setText(startAction + startCharge.toLower() + nameModule );
        }
    }

    if(numMode == RS::LiOnDischarge || numMode == RS::PbACDischarge)
    {
        if(ui->powerButton->isChecked())
        {
            dialog->setText(stopAction + startDischarge.toLower()+ nameModule );
        }else{
            dialog->setText(startAction + startDischarge.toLower()+ nameModule );        }
    }

    dialog->show();
    return dialog->exec();
}

void ControlModuleWidget::settingTextButtons()
{
    if(numMode == RS::LiOnChargeBalanceAll ||
            numMode == RS::LiOnChargeBalanceModule ||
            numMode == RS::LiOnChargeNoBalanceAll ||
            numMode == RS::LiOnChargeNoBalanceModule ||
            numMode == RS::PbACChargeBalanceAll ||
            numMode == RS::PbACChargeBalanceModule ||
            numMode == RS::PbACChargeNoBalanceAll ||
            numMode == RS::PbACChargeNoBalanceModule)
    {
        if(ui->powerButton->isChecked())
        {
            ui->powerButton->setText(stopAction + startCharge.toLower());
        }else{
            ui->powerButton->setText(startCharge);
        }

        ui->AddString->setText(addStringCharge);
    }

    if(numMode == RS::LiOnDischarge || numMode == RS::PbACDischarge)
    {
        if(ui->powerButton->isChecked())
        {
            ui->powerButton->setText(stopAction + startDischarge.toLower());
        }else{
            ui->powerButton->setText(startDischarge);
        }

        ui->AddString->setText(addStringDischarge);
    }
}

void ControlModuleWidget::showOneBtnDialog(int capp, int time)
{
    DialogOneBtn *dlg = new DialogOneBtn();
    QString text;

    if(numMode == RS::LiOnDischarge || numMode == RS::PbACDischarge)
    {
        text.append("Разряд окончен\nСнятая ёмкость: ");
    }
    else
    {
        text.append("Заряд окончен\nВкачанная ёмкость: ");
    }
    unsigned int min = time/60%60;
    unsigned int hours = time/60/60;
    text.append(QString::number(capp,'i',0) + " А/ч\n");

    text.append("Время с начала работы: " + QString::number(hours,'i',0)+" ч. " +
                QString::number(min,'i',0)+" м.");
    dlg->setText(text);
    dlg->show();
    dlg->exec();

}
