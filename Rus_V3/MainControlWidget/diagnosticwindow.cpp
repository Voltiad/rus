#include "diagnosticwindow.h"
#include "ui_diagnosticwindow.h"
#include "RusNamespace.h"
#include <QDebug>
#include <QGraphicsDropShadowEffect>

DiagnosticWindow::DiagnosticWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DiagnosticWindow)
{
    ui->setupUi(this);


    QGraphicsDropShadowEffect *ShadowBtn;
    ShadowBtn = new QGraphicsDropShadowEffect();
    ShadowBtn->setBlurRadius(BlurRadiusShadow);
    ShadowBtn->setColor(ColorShadow);
    ShadowBtn->setOffset(OffsetShadow);
    ui->close->setGraphicsEffect(ShadowBtn);

}

DiagnosticWindow::~DiagnosticWindow()
{
    delete ui;
}

void DiagnosticWindow::on_close_clicked()
{
    this->hide();
}

void DiagnosticWindow::updateInfo()
{
    infoText.clear();

    updateVisibleButtons();

    testConnectionCableCharge();
    testValidBatt();
    testStatusBatt();

    ui->infoLabel->setText(infoText);
}

void DiagnosticWindow::updateVisibleButtons()
{
    switch (getNumMode())
    {
    case RS::LiOnChargeBalanceAll:
        visibleLabel(ui->lblBms,ui->lblBmsConnection,true);
        visibleLabel(ui->lblCharge,ui->lblChardgConnection,true);
        visibleLabel(ui->lblMc,ui->lblMcConnection,false);
        break;

    case RS::LiOnChargeNoBalanceAll:
        visibleLabel(ui->lblBms,ui->lblBmsConnection,true);
        visibleLabel(ui->lblCharge,ui->lblChardgConnection,true);
        visibleLabel(ui->lblMc,ui->lblMcConnection,false);
        break;

    case RS::LiOnChargeBalanceModule:
        visibleLabel(ui->lblBms,ui->lblBmsConnection,false);
        visibleLabel(ui->lblCharge,ui->lblChardgConnection,true);
        visibleLabel(ui->lblMc,ui->lblMcConnection,true);
        break;

    case RS::LiOnChargeNoBalanceModule:
        visibleLabel(ui->lblBms,ui->lblBmsConnection,false);
        visibleLabel(ui->lblCharge,ui->lblChardgConnection,true);
        visibleLabel(ui->lblMc,ui->lblMcConnection,true);
        break;

    case RS::LiOnDischarge:
        visibleLabel(ui->lblBms,ui->lblBmsConnection,false);
        visibleLabel(ui->lblCharge,ui->lblChardgConnection,false);
        visibleLabel(ui->lblMc,ui->lblMcConnection,true);
        break;

    default:
        visibleLabel(ui->lblBms,ui->lblBmsConnection,true);
        visibleLabel(ui->lblCharge,ui->lblChardgConnection,true);
        visibleLabel(ui->lblMc,ui->lblMcConnection,true);
        break;
    }

    connectionStatusDevice(dsCharger,ui->lblChardgConnection);
    connectionStatusDevice(dsMoudCont,ui->lblMcConnection);
    connectionStatusDevice(dsBMS,ui->lblBmsConnection);

}

void DiagnosticWindow::visibleLabel(QLabel *lblName,
                                    QLabel *lblValue,
                                    bool value)
{
    lblName->setVisible(value);
    lblValue->setVisible(value);
}

void DiagnosticWindow::testValidBatt()
{
    bool flagValid = true;
    for (int var = minRange; var < maxRange; ++var)
    {
        if((dsBmsBattery->cell[var].infoReg&32768) == 0){
            flagValid = false;
        }
    }

    if(flagValid == false){
        infoText.append(noConnectionBMS);
        flags->connectionStatus = false;
    }else{
        flags->connectionStatus= true;
    }

}

void DiagnosticWindow::testStatusBatt()
{
    bool flagEmergency = false;
    bool flagWarning = false;
    for (int var = minRange; var < maxRange; ++var)
    {
        if(dsBmsBattery->cell[var].flagErrors == RS::Emergency)
        {
            flagEmergency = true;

        }
        else if(dsBmsBattery->cell[var].flagErrors == RS::Warning)
        {
            flagWarning = true;
        }
    }

    if(flagEmergency){
        infoText.append(emergencyText);
        flags->flagErrorsBatt = RS::Emergency;
    }else if(flagWarning){
        infoText.append(warningState);
        flags->flagErrorsBatt = RS::Warning;
    }else{
        flags->flagErrorsBatt = RS::Normal;
    }

}

void DiagnosticWindow::testConnectionCableCharge()
{
    if((getNumMode() == RS::LiOnDischarge)||(getNumMode() == RS::PbACDischarge)){

    }else
    {
        bool cntChrg = dsCharger->connectionStatus;
        bool thirdDevice = dsBMS->connectionStatus || dsMoudCont->connectionStatus;
        bool voltageOnExit = dsCharger->voltageOne < 500;

        if((cntChrg && thirdDevice) && voltageOnExit)
        {
            infoText.append(noConnectionForceCable);
        }
    }
}

int DiagnosticWindow::statusSystem()
{
    return -1;
}
