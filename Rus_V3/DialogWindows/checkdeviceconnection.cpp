#include "checkdeviceconnection.h"
#include "ui_checkdeviceconnection.h"
#include <QGraphicsDropShadowEffect>
#include <QDebug>
#include "RusNamespace.h"

checkDeviceConnection::checkDeviceConnection(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::checkDeviceConnection)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint |
                         Qt::WindowStaysOnTopHint);

    ui->pushButton->setDisabled(false);

    QGraphicsDropShadowEffect *ShadowBtn = new QGraphicsDropShadowEffect();
    ShadowBtn->setBlurRadius(BlurRadiusShadow);
    ShadowBtn->setColor(ColorShadow);
    ShadowBtn->setOffset(OffsetShadow);
    ui->pushButton->setGraphicsEffect(ShadowBtn);

    QGraphicsDropShadowEffect *Shadow = new QGraphicsDropShadowEffect();
    Shadow->setBlurRadius(BlurRadiusShadow);
    Shadow->setColor(ColorShadow);
    Shadow->setOffset(OffsetShadow);
    ui->btnClsoe->setGraphicsEffect(Shadow);

    checkSec = 3;
    countDown = checkSec +1;
}

checkDeviceConnection::~checkDeviceConnection()
{
    delete ui;
}

void checkDeviceConnection::setCheckSec(int value)
{
    checkSec = value;
}

void checkDeviceConnection::updateStatus(QString text)
{
    ui->label->setText(text);
}

void checkDeviceConnection::updateWidget()
{


}

void checkDeviceConnection::on_pushButton_clicked()
{
    this->accept();
}

void checkDeviceConnection::on_btnClsoe_clicked()
{
    this->close();
}
