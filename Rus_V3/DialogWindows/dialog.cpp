#include "dialog.h"
#include "ui_dialog.h"
#include "QGraphicsDropShadowEffect"
#include "RusNamespace.h"

Dialog::Dialog(QString text) :
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->label->setText(text);

    this->setWindowFlags(Qt::CustomizeWindowHint|Qt::WindowStaysOnTopHint);

    connect(ui->btnYes,SIGNAL(clicked()),this,SLOT(clickYesButton()));
    connect(ui->btnNo,SIGNAL(clicked()),this,SLOT(clickNoButton()));


    QGraphicsDropShadowEffect *ShadowBtn[2];
    for(int i=0;i<2;i++)
    {
        ShadowBtn[i] = new QGraphicsDropShadowEffect();
        ShadowBtn[i]->setBlurRadius(BlurRadiusShadow);
        ShadowBtn[i]->setColor(ColorShadow);
        ShadowBtn[i]->setOffset(OffsetShadow);

    }

    ui->btnYes->setGraphicsEffect(ShadowBtn[0]);
    ui->btnNo->setGraphicsEffect(ShadowBtn[1]);
}

Dialog::~Dialog()
{
    delete ui;
}
void Dialog::setText(QString text){
    ui->label->setText(text);
}

void Dialog::clickYesButton(){
    emit answerDialog(true);
    this->accept();
}

void Dialog::clickNoButton(){
    emit answerDialog(false);
    this->close();
}
