#ifndef DIALOG_H
#define DIALOG_H


#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QString = "");
    ~Dialog();

public slots:
    void setText(QString);

private slots:
    void clickYesButton();
    void clickNoButton();

signals:
    void answerDialog(bool);


private:
    Ui::Dialog *ui;

};


#endif // DIALOG_H
