#ifndef DIALOGONEBTN_H
#define DIALOGONEBTN_H

#include <QDialog>

namespace Ui {
class DialogOneBtn;
}

class DialogOneBtn : public QDialog
{
    Q_OBJECT

public:
    explicit DialogOneBtn(QString = "");
    ~DialogOneBtn();

private:
    Ui::DialogOneBtn *ui;

public slots:
    void setText(QString);

    void setBtnText(QString);

private slots:
    void clickYesButton();
};

#endif // DIALOGONEBTN_H
