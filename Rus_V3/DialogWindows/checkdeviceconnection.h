#ifndef CHECKDEVICECONNECTION_H
#define CHECKDEVICECONNECTION_H

#include <QDialog>

namespace Ui {
class checkDeviceConnection;
}

class checkDeviceConnection : public QDialog
{
    Q_OBJECT

public:
    explicit checkDeviceConnection(QWidget *parent = 0);
    ~checkDeviceConnection();

private:
    Ui::checkDeviceConnection *ui;

    int countDown;

public:
    int checkSec;
    void setCheckSec(int value);
    bool flagChargeOAB;
    void setChargeOAB(bool value){flagChargeOAB = value;}
    bool getChargeOAB(){return flagChargeOAB;}

    bool flagChargeVAB1;
    void setChargeVAB1(bool value){flagChargeVAB1 = value;}
    bool getChargeVAB1(){return flagChargeVAB1;}


    bool flagChargeVAB2;
    void setChargeVAB2(bool value){flagChargeVAB2 = value;}
    bool getChargeVAB2(){return flagChargeVAB2;}

    bool flagMCOAB;
    void setMCOAB(bool value){flagMCOAB = value;}
    bool getMCOAB(){return flagMCOAB;}

    bool flagMCVAB1;
    void setMCVAB1(bool value){flagMCVAB1 = value;}
    bool getMCVAB1(){return flagMCVAB1;}


    bool flagMCVAB2;
    void setMCVAB2(bool value){flagMCVAB2 = value;}
    bool getMCVAB2(){return flagMCVAB2;}

    bool flagBMS;
    void setBMS(bool value){flagBMS = value;}
    bool getBMS(){return flagBMS;}

public:
    void updateStatus(QString text);

public slots:
    void updateWidget();
private slots:
    void on_pushButton_clicked();
    void on_btnClsoe_clicked();
};

#endif // CHECKDEVICECONNECTION_H
