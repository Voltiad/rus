#include "dialogonebtn.h"
#include "ui_dialogonebtn.h"
#include "QGraphicsDropShadowEffect"
#include "RusNamespace.h"

DialogOneBtn::DialogOneBtn(QString text) :
    ui(new Ui::DialogOneBtn)
{

    ui->setupUi(this);
    ui->label->setText(text);

    this->setWindowFlags(Qt::CustomizeWindowHint|Qt::WindowStaysOnTopHint);

    connect(ui->btnYes,SIGNAL(clicked()),this,SLOT(clickYesButton()));
    QGraphicsDropShadowEffect *ShadowBtn[2];
    for(int i=0;i<2;i++)
    {
        ShadowBtn[i] = new QGraphicsDropShadowEffect();
        ShadowBtn[i]->setBlurRadius(BlurRadiusShadow);
        ShadowBtn[i]->setColor(ColorShadow);
        ShadowBtn[i]->setOffset(OffsetShadow);
    }
    ui->btnYes->setGraphicsEffect(ShadowBtn[0]);

}

DialogOneBtn::~DialogOneBtn()
{
    delete ui;
}
void DialogOneBtn::setText(QString text){
    ui->label->setText(text);
}

void DialogOneBtn::setBtnText(QString text)
{
    ui->btnYes->setDisabled(true);
    ui->btnYes->setText(text);
}
void DialogOneBtn::clickYesButton(){
    this->close();
}
