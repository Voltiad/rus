#ifndef RUSNAMESPACE_H
#define RUSNAMESPACE_H

#include <QString>
#include <QDebug>
#include <QStringList>
#include <QColor>
#include <qnamespace.h>

class RS{

public:

    Q_ENUMS(WorkMode)
    Q_ENUMS(Echo)

    enum WorkMode{
        LiOnChargeBalanceAll        = 0,
        LiOnChargeNoBalanceAll      = 1,
        LiOnDischarge               = 2,
        LiOnChargeBalanceModule     = 3,
        LiOnChargeNoBalanceModule   = 4,
        PbACChargeBalanceAll        = 5,
        PbACChargeNoBalanceAll      = 6,
        PbACDischarge               = 7,
        PbACChargeBalanceModule     = 8,
        PbACChargeNoBalanceModule   = 9,
        VoltageSource               = 10,
        ResetPreset                 = 11
    };

    enum Echo{
        echoON  = 1,
        echoOFF = 0
    };

    enum WarMode{
        Normal        = 1,
        Warning       = 2,
        Emergency     = 4
    };

};


const QString stopAction = "Остановить\n";
const QString startAction = "Подтвердить\n";
const QString addStringCharge = "Вкачанная ёмкость, А/ч ";
const QString addStringDischarge = "Снятая ёмкость, А/ч";
const QString startCharge = "Заряд";
const QString startDischarge = "Разряд";

const qreal BlurRadiusShadow = 9.0;
const qreal OffsetShadow = 4.0;
const QColor ColorShadow(0, 0, 0, 160);

const QString PATH_TW = "/home/captain/Desktop";

const QString PATH_JSON = "/Json";

const QString CONFIG_FILE_NAME =  "/ComConfig.json";
const QString CONFIG_MESSAGE_FILE = "/MessageConfig.json";
const QString TOTAL_WORK_TIME = "/TotalWorkTime.json";
const QString CRASH_FILE = "/CrashFile.json";
const QString PRESETES_FILE = "/Presets.json";

const QString COM_1_JsonName =  "ttyS4";
const int adrMcOAB = 0x7C;
const int adrMcOVAB1 = 0x7D;
const int defInoRegMC = 3;
//Модуль измерения ОАБ и ВАБ1
//Адреса МИ ОАБ - 0x7C (124) , МИ ВАБ 1 - 0x7D (125)

const QString COM_2_JsonName =  "ttyS5";
const int adrCharOAB = 0x3D;
const int adrCharOAB1 = 0x3E;
const int adrCharVAB1 = 0x1E;
const int adrCharVAB2 = 0x1F;
const int startAdrCharge = 1000;
const int defInoRegCharge = 10;
//Блоки заряда - ОАБ,ОАБ1,ВАБ 1,ВАБ2
//Адреса ОАБ - 0x97 (61) , ОАБ 1 - 0x98 (62),
// ВАБ 1 - 0x49 (31), ВАБ 2 - 0x48 (30)


const QString COM_3_JsonName =  "ttyS6";
const int adrBMS = 0x7B;
const int startAdrBMSBat = 1000;
const int defInoRegBMSBat = 5;
const int startAdrBMS = 1320;
const int defInoRegBMS = 8;
//СКУ
//Адреса СКУ - 0x7B (123)

const QString COM_4_JsonName =  "ttyS7";
const int adrMcVAB2 = 0x7D;
//Модуль измерения ВАБ 2
//Адреса МИ ВАБ 2 - 0x7D (125)


const QString dateFormat = "dd.MM.yyyy";
const QString flashMountPath = "/home/captain/mnt/flash";
const QString coreMainReportPath = "/home/captain/Desktop/ReportGeneralBatt";
const QString coreBatteryReportPath = "/home/captain/Desktop/ReportModuleBatt";
const QString activeScriptUpload = "sh /home/captain/Desktop/uploadScrips.sh";

const QString Text_OAB = "ОАБ";
const QString Text_VAB1 = "ВАБ 1";
const QString Text_VAB2 = "ВАБ 2";

const QString text_balance = "Балансировка";

const QString connectionYes = "Есть связь";
const QString connectionNo = "Нет связи";

const QString textColorGreen = "color:green";
const QString textColorRed = "color:red";

const QString buttonNormal = "color:black; background-color:white";
const QString buttonReady = "color:white; background-color: rgb(93, 232, 0);";
const QString buttonEmergency = "color:white; background-color:red";


const QString emergencyText = "В системе присутсвует критическая неисправность.\n";
const QString warningState = "Предупредительное состояние системы.\n";
const QString noConnectionForceCableShort = "Силовой кабель не подключен";
const QString noConnectionForceCable = noConnectionForceCableShort + ".\n";
const QString noConnectionBMSShort = "Нет связи с СКУ";
const QString noConnectionBMS = noConnectionBMSShort + "Нет связи СКУ контейнера.\n";
const QString emergencyTextShort = "Неисправно";




#endif // RUSNAMESPACE_H
