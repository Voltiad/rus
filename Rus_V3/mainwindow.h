#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QButtonGroup>
#include <QDebug>
#include <QPushButton>
#include <QGraphicsDropShadowEffect>
#include <QThread>
#include <QProcess>
#include <QMutex>


#include "RusNamespace.h"
#include "maincontrolwidget.h"
#include "dialog.h"
#include "dialogonebtn.h"
#include "checkdeviceconnection.h"
#include "timesettings.h"

#include "mainpostman.h"
#include "chargemodule.h"
#include "modulecontrolevab2.h"
#include "modulecontroloabvab.h"
#include "bms.h"

#include "datastore.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void generalObject();
    void generalSettings();
    void lateSettings();

    int oneSecTimer;
    void timerEvent(QTimerEvent *);
    void keyPressEvent(QKeyEvent *event);
signals:
    void oneSecSignal();
private:
    Ui::MainWindow *ui;

private:
    void settingWindow();

private:
    dsChargeModule *dsChOAB;
    dsChargeModule *dsChVAB1;
    dsChargeModule *dsChVAB2;

    dsModuleControle *dsMcOAB;
    dsModuleControle *dsMcVAB1;
    dsModuleControle *dsMcVAB2;
    dsBattery *dsMcBattery;

    dsBattManagSystem *dsBMS;
    dsBattery *dsBmsBattery;

    void settingDataStore();

private:
    void settingPostmanTest();

private:
    int currentWorktime;
    int timerCurrWorkTime;

    void settingWorkTime();
    void updateWorktimeFile();


private:
    QButtonGroup *choiceWorkMode;

    void settingButtons();
    void addingButtons();
    void connectingButtons();
    void settingShadows();

signals:
    void updatePreset(int index);



private:
    MainPostMan *mcOABnVAB1;
    MainPostMan *chargeDevice;
    MainPostMan *bms;
    MainPostMan *mcVAB2;

    void settingConnection();

    void settingModbus();
    void createPostmanObjects();
    void connectThreads();

    void settingPostman();
    void connectModbusDatas();
    void TEMP_DATAS();


private:
    MainControlWidget *liOnChargeBalanceAll;
    MainControlWidget *liOnChargeNoBalanceAll;
    MainControlWidget *liOnDischarge;
    MainControlWidget *liOnChargeBalanceModule;
    MainControlWidget *liOnChargeNoBalanceModule;
    MainControlWidget *pbACChargeBalanceAll;
    MainControlWidget *pbACChargeNoBalanceAll;
    MainControlWidget *pbACDischarge;
    MainControlWidget *pbACChargeBalanceModule;
    MainControlWidget *pbACChargeNoBalanceModule;
    MainControlWidget *voltageSource;

    QList<MainControlWidget *> controlWidgets;

    void settingWorkModeWindow();

    void createWindowObjects();
    void fillingWindowContainer();
    void connectCurrentWorkTimeWidget();
    void startBlockWidgets();
    void connectPowerButtons();
    void chargeMode(int index);
    void dischargeMode(int index);
    void connectChargeModes(int index);
    void connectDischargeModes(int index);
    void connectPresets();
    void settingDatasModeWindow();


private:
    void settingLoadWindows();
    void connetChangeWindows();

private:
    int currentWorkMode;

private slots:
    void loadWorkModeWidget(int numMode);
    void setCurrentWorkMode(int workMode){ currentWorkMode = workMode;}
    int getCurrentWorkMode(){return currentWorkMode;}

private:
    checkDeviceConnection *deviceConnection;
    bool answerPerson;
    void checkConnection(int num);
    void changeWindows();
    void openWindow();
    void changeHeaderText();


private:
    int updateDatas;
    void updatePresets(int numMode);

    void startTimersDevice(int num);
    void startDevice(MainPostMan *device,int msec);
    void stopDevice();

private slots:
    void updateDisplayDatas(int num);

private:
    void settingCrashFileProgramm();


private:
    Dialog *dialog;
    DialogOneBtn *dialogOneBtn;
    bool answer;
private slots:
    inline void answerDialog(bool);

private slots:
    void closeProgramm();



private:
    int timerChargeDevice;
    int updateTimerMsec;
    int readyCurrent;
signals:
    void updateCurrentWorkTime(int value);

private:
    TimeSettings *timeSettings;
    void settingTime();
};


inline void MainWindow::answerDialog(bool answ){

    answer=answ;
    dialog->hide();
}

#endif // MAINWINDOW_H
