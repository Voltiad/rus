#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "controlwindow.h"
#include "controlmodulewidget.h"
#include "maincontrolwidget.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    generalObject();
    generalSettings();
    lateSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::generalObject()
{
    ui->setupUi(this);

    deviceConnection = new checkDeviceConnection();
    deviceConnection->setModal(true);

}

void MainWindow::generalSettings()
{
    settingDataStore();

    settingPostmanTest();

    settingWindow();

    settingButtons();

    settingConnection();

    settingWorkModeWindow();

    settingLoadWindows();

    settingCrashFileProgramm();

    settingWorkTime();

    settingTime();
}

void MainWindow::lateSettings()
{
    emit updatePreset(RS::ResetPreset);
    setCurrentWorkMode(RS::ResetPreset);
}


void MainWindow::timerEvent(QTimerEvent *event)
{
    int eventID = event->timerId();

    if(eventID == timerCurrWorkTime)
    {
        currentWorktime++;
////        updateWorktimeFile();
        updateCurrentWorkTime(currentWorktime);
    }

    if(eventID == oneSecTimer)
    {
        emit oneSecSignal();
    }
}


void MainWindow::keyPressEvent(QKeyEvent *event)
{
    int pressedKey = event->key();

    switch (pressedKey) {
    case Qt::Key_Enter:
        closeProgramm();
        break;
    default:
        break;
    }
}


void MainWindow::settingWindow()
{
    this->setWindowFlags(Qt::CustomizeWindowHint);

    dialog = new Dialog();

    connect(dialog,SIGNAL(answerDialog(bool)),
            this,SLOT(answerDialog(bool)));

    dialogOneBtn = new DialogOneBtn();
}


void MainWindow::settingDataStore()
{
    dsChOAB = new dsChargeModule;
    dsChVAB1 = new dsChargeModule;
    dsChVAB2 = new dsChargeModule;

    dsMcOAB = new dsModuleControle;
    dsMcVAB1 = new dsModuleControle;
    dsMcVAB2 = new dsModuleControle;
    dsMcBattery = new dsBattery;

    dsBMS = new dsBattManagSystem;
    dsBmsBattery = new dsBattery;
}


void MainWindow::settingPostmanTest()
{

}



void MainWindow::settingWorkTime()
{
//    QString path = PATH_TW
//            + PATH_JSON + TOTAL_WORK_TIME;
//    QFile configFile (path);
//    QJsonDocument jsonDoc;

    currentWorktime = 0;

//    if(configFile.exists())
//    {
//        configFile.open(QFile::ReadOnly);

//        jsonDoc = QJsonDocument().fromJson(configFile.readAll());
//        QJsonObject obj =jsonDoc.object();

//        currentWorktime = obj.value("TotalWorkTime").toInt();

//        configFile.close();
//    }
//    else{
//        configFile.open(QIODevice::WriteOnly);
//        QByteArray data = "{\n\t\"TotalWorkTime\":0\n}";
//        configFile.write(data);
//        configFile.close();
//    }

   timerCurrWorkTime = startTimer(1000);
}

void MainWindow::updateWorktimeFile()
{
//    QString path = PATH_TW
//            + PATH_JSON + TOTAL_WORK_TIME;
//    QFile configFile (path);

//    QByteArray data = "{\n\t\"TotalWorkTime\":"
//            + QByteArray::number(currentWorktime) +"\n}";

//    configFile.open(QIODevice::ReadWrite);
//    configFile.resize(0);
//    configFile.write(data);
//    configFile.close();
}



void MainWindow::settingButtons()
{
    addingButtons();
    settingShadows();
}

void MainWindow::addingButtons()
{
    choiceWorkMode = new QButtonGroup();

    choiceWorkMode->addButton(ui->LiOnChargeBalanceAll,
                              RS::LiOnChargeBalanceAll);

    choiceWorkMode->addButton(ui->LiOnChargeNoBalanceAll,
                              RS::LiOnChargeNoBalanceAll);

    choiceWorkMode->addButton(ui->LiOnDischargeAll,
                              RS::LiOnDischarge);

    choiceWorkMode->addButton(ui->LiOnChargeBalanceModule,
                              RS::LiOnChargeBalanceModule);

    choiceWorkMode->addButton(ui->LiOnChargeNoBalanceModule,
                              RS::LiOnChargeNoBalanceModule);

    choiceWorkMode->addButton(ui->PbAcChargeNoBalanceAll,
                              RS::PbACChargeNoBalanceAll);

    choiceWorkMode->addButton(ui->PbAcChargeNoBalanceAll,
                              RS::PbACChargeNoBalanceAll);

    choiceWorkMode->addButton(ui->PbAcDischargeAll,
                              RS::PbACDischarge);

    choiceWorkMode->addButton(ui->PbAcChargeNoBalanceModule,
                              RS::PbACChargeNoBalanceModule);

    choiceWorkMode->addButton(ui->PbAcChargeNoBalanceModule,
                              RS::PbACChargeNoBalanceModule);

    choiceWorkMode->addButton(ui->VoltageMode,
                              RS::VoltageSource);



//    choiceWorkMode->button(RS::LiOnChargeBalanceAll)->setDisabled(true);

//    choiceWorkMode->button(RS::LiOnChargeBalanceModule)->setDisabled(true);

//    choiceWorkMode->button(RS::LiOnDischarge)->setDisabled(true);

//    choiceWorkMode->button(RS::LiOnChargeNoBalanceAll)->setDisabled(true);


    choiceWorkMode->button(RS::PbACChargeNoBalanceAll)->setDisabled(true);

    choiceWorkMode->button(RS::PbACChargeNoBalanceAll)->setDisabled(true);

    choiceWorkMode->button(RS::PbACDischarge)->setDisabled(true);


    choiceWorkMode->button(RS::PbACChargeNoBalanceAll)->setDisabled(true);

    choiceWorkMode->button(RS::PbACChargeNoBalanceModule)->setDisabled(true);

    choiceWorkMode->button(RS::VoltageSource)->setDisabled(true);

//    ui->generateReport->setDisabled(true);

    choiceWorkMode->setExclusive(false);

}

void MainWindow::settingShadows()
{
    QGraphicsDropShadowEffect *ShadowBtn[14];
    for(int i=0;i<14;i++)
    {
        ShadowBtn[i] = new QGraphicsDropShadowEffect();
        ShadowBtn[i]->setBlurRadius(BlurRadiusShadow);
        ShadowBtn[i]->setColor(ColorShadow);
        ShadowBtn[i]->setOffset(OffsetShadow);
    }

    for (int var = 0; var < 11 ; ++var) {
        if(var == RS::PbACChargeBalanceAll||
                var == RS::PbACChargeBalanceModule){

        }else{
            choiceWorkMode->button(var)->
                    setGraphicsEffect(ShadowBtn[var]);
        }
    }
    ui->settingDateTime->setGraphicsEffect(ShadowBtn[12]);
    ui->generateReport->setGraphicsEffect(ShadowBtn[13]);

}



void MainWindow::settingConnection()
{
    settingModbus();
    settingPostman();
}

void MainWindow::settingModbus()
{
    createPostmanObjects();
    connectThreads();
}

void MainWindow::createPostmanObjects()
{
    moduleControlOABVAB *modContOABVAB =
            new moduleControlOABVAB(COM_1_JsonName);

    ChargeModule *chargeModule =
            new ChargeModule(COM_2_JsonName);

    BMS *sku = new BMS(COM_3_JsonName);

    moduleControleVAB2 *modContrVAB2 =
            new moduleControleVAB2(COM_4_JsonName);

    mcOABnVAB1 = modContOABVAB;
    chargeDevice = chargeModule;
    bms = sku;
    mcVAB2 = modContrVAB2;
}

void MainWindow::connectThreads()
{
    QThread *threadCom1 = new QThread();
    QThread *threadCom2 = new QThread();
    QThread *threadCom3 = new QThread();
    QThread *threadCom4 = new QThread();


    mcOABnVAB1->moveToThread(threadCom1);
    chargeDevice->moveToThread(threadCom2);
    bms->moveToThread(threadCom3);
    mcVAB2->moveToThread(threadCom4);

    connect(threadCom1,SIGNAL(started()),mcOABnVAB1,SLOT(loop()));
    connect(threadCom2,SIGNAL(started()),chargeDevice,SLOT(loop()));
    connect(threadCom3,SIGNAL(started()),bms,SLOT(loop()));
    connect(threadCom4,SIGNAL(started()),mcVAB2,SLOT(loop()));

    threadCom1->start();
    threadCom2->start();
    threadCom3->start();
    threadCom4->start();
}

void MainWindow::settingPostman()
{
    connectModbusDatas();
   // TEMP_DATAS();
}

void MainWindow::connectModbusDatas()
{
    chargeDevice->chargerOAB = dsChOAB;
    chargeDevice->chargerVAB1 = dsChVAB1;
    chargeDevice->chargerVAB2 = dsChVAB2;

    mcOABnVAB1->moduleCOAB = dsMcOAB;
    mcOABnVAB1->moduleCVAB1 = dsMcVAB1;
    mcVAB2->moduleCVAB2 = dsMcVAB2;

    mcOABnVAB1->moduleControleBattery = dsBmsBattery;
    mcVAB2->moduleControleBattery = mcOABnVAB1->moduleControleBattery;

    bms->battManagSystem = dsBMS;
    bms->bmsBattery = dsBmsBattery;
}

void MainWindow::TEMP_DATAS()
{

}


void MainWindow::settingWorkModeWindow()
{
    createWindowObjects();
    connectCurrentWorkTimeWidget();
    startBlockWidgets();
    connectPowerButtons();
    connectPresets();
    settingDatasModeWindow();
}

void MainWindow::createWindowObjects()
{
    liOnChargeBalanceAll        = new MainControlWidget();
    liOnChargeNoBalanceAll      = new MainControlWidget();
    liOnDischarge               = new MainControlWidget();
    liOnChargeBalanceModule     = new MainControlWidget();
    liOnChargeNoBalanceModule   = new MainControlWidget();
    pbACChargeBalanceAll        = new MainControlWidget();
    pbACChargeNoBalanceAll      = new MainControlWidget();
    pbACDischarge               = new MainControlWidget();
    pbACChargeBalanceModule     = new MainControlWidget();
    pbACChargeNoBalanceModule   = new MainControlWidget();
    voltageSource               = new MainControlWidget();

    fillingWindowContainer();
}

void MainWindow::fillingWindowContainer()
{
    controlWidgets.append(liOnChargeBalanceAll);
    controlWidgets.append(liOnChargeNoBalanceAll);
    controlWidgets.append(liOnDischarge);
    controlWidgets.append(liOnChargeBalanceModule);
    controlWidgets.append(liOnChargeNoBalanceModule);
    controlWidgets.append(pbACChargeBalanceAll);
    controlWidgets.append(pbACChargeNoBalanceAll);
    controlWidgets.append(pbACDischarge);
    controlWidgets.append(pbACChargeBalanceModule);
    controlWidgets.append(pbACChargeNoBalanceModule);
    controlWidgets.append(voltageSource);
}

void MainWindow::connectCurrentWorkTimeWidget()
{
    for (int var = 0; var < controlWidgets.count(); ++var) {
        connect(this,SIGNAL(updateCurrentWorkTime(int)),
               controlWidgets[var],SLOT(updateWorkTimeLbl(int)));
    }

}

void MainWindow::startBlockWidgets()
{

}

void MainWindow::connectPowerButtons()
{
    chargeMode(RS::LiOnChargeBalanceAll);
    chargeMode(RS::LiOnChargeNoBalanceAll);
    chargeMode(RS::LiOnChargeBalanceModule);
    chargeMode(RS::LiOnChargeNoBalanceModule);
    chargeMode(RS::PbACChargeNoBalanceAll);
    chargeMode(RS::PbACChargeNoBalanceModule);
    chargeMode(RS::VoltageSource);

    dischargeMode(RS::LiOnDischarge);
    dischargeMode(RS::PbACDischarge);

}

void MainWindow::chargeMode(int index)
{
        connect(controlWidgets[index],&MainControlWidget::powerOnOAB,
                chargeDevice,&MainPostMan::startOAB);

        connect(controlWidgets[index],&MainControlWidget::powerOnVAB1,
                chargeDevice,&MainPostMan::startVAB1);

        connect(controlWidgets[index],&MainControlWidget::powerOnVAB2,
                chargeDevice,&MainPostMan::startVAB2);

        connect(controlWidgets[index],&MainControlWidget::powerOffOAB,
                chargeDevice,&MainPostMan::stopOAB);

        connect(controlWidgets[index],&MainControlWidget::powerOffVAB1,
                chargeDevice,&MainPostMan::stopVAB1);

        connect(controlWidgets[index],&MainControlWidget::powerOffVAB2,
                chargeDevice,&MainPostMan::stopVAB2);

        connect(controlWidgets[index],&MainControlWidget::updateVoltageOAB,
                chargeDevice,&MainPostMan::setVltOAB);

        connect(controlWidgets[index],&MainControlWidget::updateVoltageVAB1,
                chargeDevice,&MainPostMan::setVltVAB1);

        connect(controlWidgets[index],&MainControlWidget::updateVoltageVAB2,
                chargeDevice,&MainPostMan::setVltVAB2);


        connect(controlWidgets[index],&MainControlWidget::startBalanceOAB,
                 mcOABnVAB1,&MainPostMan::startBalanceOAB);
        connect(controlWidgets[index],&MainControlWidget::startBalanceOAB,
                 bms,&MainPostMan::startBalanceBMS);

        connect(controlWidgets[index],&MainControlWidget::startBalanceVAB1,
                 mcOABnVAB1,&MainPostMan::startBalanceVAB1);
        connect(controlWidgets[index],&MainControlWidget::startBalanceVAB1,
                 bms,&MainPostMan::startBalanceBMS);

        connect(controlWidgets[index],&MainControlWidget::startBalanceVAB2,
                 mcVAB2,&MainPostMan::startBalanceVAB2);
        connect(controlWidgets[index],&MainControlWidget::startBalanceVAB2,
                 bms,&MainPostMan::startBalanceBMS);

        connect(controlWidgets[index],&MainControlWidget::stopBalanceOAB,
                 mcOABnVAB1,&MainPostMan::stopBalanceOAB);
        connect(controlWidgets[index],&MainControlWidget::stopBalanceOAB,
                 bms,&MainPostMan::stopBalanceBMS);

        connect(controlWidgets[index],&MainControlWidget::stopBalanceVAB1,
                 mcOABnVAB1,&MainPostMan::stopBalanceVAB1);
        connect(controlWidgets[index],&MainControlWidget::stopBalanceVAB1,
                 bms,&MainPostMan::stopBalanceBMS);

        connect(controlWidgets[index],&MainControlWidget::stopBalanceVAB2,
                 mcVAB2,&MainPostMan::stopBalanceVAB2);
        connect(controlWidgets[index],&MainControlWidget::stopBalanceVAB2,
                 bms,&MainPostMan::stopBalanceBMS);

}

void MainWindow::dischargeMode(int index)
{
    connect(controlWidgets[index],&MainControlWidget::powerOnOAB,
            mcOABnVAB1,&MainPostMan::startDischargeOAB);

    connect(controlWidgets[index],&MainControlWidget::powerOnVAB1,
            mcOABnVAB1,&MainPostMan::startDischargeVAB1);

    connect(controlWidgets[index],&MainControlWidget::powerOnVAB2,
            mcVAB2,&MainPostMan::startDischargeVAB2);

    connect(controlWidgets[index],&MainControlWidget::powerOffOAB,
            mcOABnVAB1,&MainPostMan::stopDischargeOAB);

    connect(controlWidgets[index],&MainControlWidget::powerOffVAB1,
            mcOABnVAB1,&MainPostMan::stopDischargeVAB1);

    connect(controlWidgets[index],&MainControlWidget::powerOffVAB2,
            mcVAB2,&MainPostMan::stopDischargeVAB2);
}

void MainWindow::connectPresets()
{
    connect(this,&MainWindow::updatePreset,
            chargeDevice,&MainPostMan::updatePresets);
    connect(this,&MainWindow::updatePreset,
            bms,&MainPostMan::updatePresets);
    connect(this,&MainWindow::updatePreset,
            mcOABnVAB1,&MainPostMan::updatePresets);
    connect(this,&MainWindow::updatePreset,
            mcVAB2,&MainPostMan::updatePresets);
}

void MainWindow::settingDatasModeWindow()
{
    for (int var = 0; var < controlWidgets.count(); ++var)
    {
        controlWidgets[var]->dsOAB_disp = dsChOAB;
        controlWidgets[var]->dsVAB1_disp = dsChVAB1;
        controlWidgets[var]->dsVAB2_disp = dsChVAB2;

        controlWidgets[var]->dsMcOAB_disp = dsMcOAB;
        controlWidgets[var]->dsMcVAB1_disp = dsMcVAB1;
        controlWidgets[var]->dsMcVAB2_disp = dsMcVAB2;
        controlWidgets[var]->dsMcBatt_disp = dsBmsBattery;


        controlWidgets[var]->dsBMS_disp = dsBMS;
        controlWidgets[var]->dsBmsBattery_disp = dsBmsBattery;

        controlWidgets[var]->lateSetting();
    }
}



void MainWindow::settingLoadWindows()
{
    connetChangeWindows();
}

void MainWindow::connetChangeWindows()
{
    connect(choiceWorkMode,SIGNAL(buttonClicked(int)),
            this,SLOT(loadWorkModeWidget(int)));

    connect(choiceWorkMode,SIGNAL(buttonClicked(int)),
            this,SLOT(updateDisplayDatas(int)));

    for (int var = 0; var < controlWidgets.count(); ++var) {
       connect(controlWidgets[var],&MainControlWidget::goToWorkMode,
               [this](){
           this->showFullScreen();
           //this->show();
       });
    }

    connect(this,SIGNAL(oneSecSignal()),
            deviceConnection,SLOT(updateWidget()));
}



void MainWindow::loadWorkModeWidget(int numMode)
{
    if(numMode != getCurrentWorkMode())
    {
        checkConnection(numMode);

        if(!answerPerson) return;
        updatePresets(numMode);
        controlWidgets[numMode]->resetButtons();
    }
    setCurrentWorkMode(numMode);
    changeWindows();

}

void MainWindow::checkConnection(int num)
{
//    deviceConnection->setCheckSec(3);
//    oneSecTimer = startTimer(1000);
    deviceConnection->updateStatus("Выбрать режим:\n"+
                choiceWorkMode->button(num)->text());
    deviceConnection->show();
//    startTimersDevice(num);
    answerPerson = deviceConnection->exec();
//    oneSecTimer = 0;
}

void MainWindow::changeWindows()
{
    openWindow();
    changeHeaderText();
}

void MainWindow::openWindow()
{
    this->hide();

    int numMode = getCurrentWorkMode();

    controlWidgets[numMode]->showFullScreen();
    controlWidgets[numMode]->setCentralWidget(numMode);
    //controlWidgets[numMode]->show();


}

void MainWindow::changeHeaderText()
{
    QString text;
    text  = choiceWorkMode->button(getCurrentWorkMode())->text();
    text.replace(text.indexOf("\n"),1,"");
    text.replace(text.indexOf("\n"),1,"");
    ui->currentWorkMode->setText("Текущий режим работы: " + text);
    controlWidgets[getCurrentWorkMode()]->setHeaderText(text);
}

void MainWindow::updatePresets(int numMode)
{
    emit updatePreset(numMode);
}

void MainWindow::startTimersDevice(int num)
{
    stopDevice();
    updateDatas = startTimer(1000);
    int msc = 1000;

    chargeDevice->setNumMode(num);
    mcVAB2->setNumMode(num);
    mcOABnVAB1->setNumMode(num);
    bms->setNumMode(num);


    switch (num)
    {
    case RS::LiOnChargeBalanceAll:
            startDevice(chargeDevice,msc);
            startDevice(bms,msc);
        break;

    case RS::LiOnChargeNoBalanceAll:
            startDevice(chargeDevice,msc);
            startDevice(bms,msc);
        break;

    case RS::LiOnChargeBalanceModule:
            startDevice(chargeDevice,msc);
            startDevice(mcVAB2,msc);
            startDevice(mcOABnVAB1,msc);
        break;

    case RS::LiOnChargeNoBalanceModule:
            startDevice(chargeDevice,msc);
            startDevice(mcVAB2,msc);
            startDevice(mcOABnVAB1,msc);
        break;

    case RS::PbACDischarge:
            startDevice(mcVAB2,msc);
            startDevice(mcOABnVAB1,msc);
        break;

    case RS::LiOnDischarge:
             startDevice(mcVAB2,msc);
             startDevice(mcOABnVAB1,msc);
        break;

    default:

        break;
    }

    updateDisplayDatas(getCurrentWorkMode());
}

void MainWindow::startDevice(MainPostMan *device, int msec)
{
    device->setInterval(msec);
    device->start();
}

void MainWindow::stopDevice()
{
    mcOABnVAB1->stop();
    chargeDevice->stop();
    mcVAB2->stop();
    bms->stop();
}

void MainWindow::updateDisplayDatas(int num)
{
    for (int var = 0; var < controlWidgets.count(); ++var)
    {
        if(var == num){
            controlWidgets[var]->startDisplayTimer();

        }else{
            controlWidgets[var]->stopDisplayTimer();
        }
        readyCurrent = 0;
    }
}




void MainWindow::settingCrashFileProgramm()
{
    QString path = QCoreApplication::applicationDirPath()
            + PATH_JSON + CRASH_FILE;

    QFile configFile (path);

    for (int var = 0; var < controlWidgets.count(); ++var) {
        connect(controlWidgets[var],&MainControlWidget::closeProg,
                this,&MainWindow::closeProgramm);
    }
    if(configFile.exists()){
        qDebug() << "FAIS SYSTEM, ALL SETTINGS ARE RESET!";

        dialogOneBtn->setText("Система восстановлена\nпосле нештатного выключения");

        dialogOneBtn->setModal(true);

        dialogOneBtn->setWindowFlags(Qt::WindowStaysOnTopHint|
                                     Qt::FramelessWindowHint);
                dialogOneBtn->show();

    }else{
        configFile.open(QIODevice::WriteOnly);
        QByteArray data = "{\n\t\"CrashStatus\":-1";
        configFile.write(data);
        configFile.close();
    }

}


void MainWindow::closeProgramm()
{
    dialog->setText("Выключить компьютер?\n"
                    "Все текущиие процессы\nбудут остановлены.");

    dialog->setModal(true);
    dialog->show();
    dialog->exec();

    if(answer){
        QString path = QCoreApplication::applicationDirPath()
                + PATH_JSON + CRASH_FILE;
        QFile configFile (path);
        configFile.remove();
        qDebug() << "Fix ShutDown Sudo";

        for (int var = 0; var < controlWidgets.count(); ++var) {
            controlWidgets[var]->close();
        }
        chargeDevice->stopOAB();
        chargeDevice->stopVAB1();
        chargeDevice->stopVAB2();

        dialog->close();
        dialogOneBtn->close();
        this->close();
    }
}


void MainWindow::settingTime()
{
    timeSettings = new TimeSettings();
    connect(ui->settingDateTime,&QPushButton::clicked,
            [this](){
        this->hide();
        timeSettings->reloadData();
        timeSettings->showFullScreen();
    });

    connect(timeSettings,&TimeSettings::closeTimeSettings,
            [this](){
        timeSettings->close();
        this->showFullScreen();
    });
}

